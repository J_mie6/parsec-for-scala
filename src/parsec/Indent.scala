package parsec

/***/
trait IndentationSensitive
{
    var stateLevel: Int = 0
    var stateRequiredIndent: Int = 1
    var levelCache: List[(Int, Int)] = List((0, 1))
    
    /**
     * The level cache contains the current indentation levels encountered
     * by the parser. In the event that the indentation is not correct, the
     * level cache is used to provide an estimate of the levels that the line
     * *could* belong to. This algorithm is currently not perfect at all, and
     * should be improved to provide more meaningful suggestions.
     */
    def prettyLevelCache(current: Int) = 
    {
        var levelCache = this.levelCache
        var upperLevel = levelCache.head
        if (upperLevel._2 > current)
        {
            levelCache = levelCache.tail
            while (levelCache.head._2 > current)
            {
                upperLevel = levelCache.head
                levelCache = levelCache.tail
            }
        }
        else levelCache = levelCache.tail
        levelCache = levelCache.foldLeft(List[(Int, Int)]()){case (ls, (l, c)) => if (!ls.exists{case (l_, c) => l == l_}) ls :+ (l, c) else ls}
        (upperLevel::levelCache).foldRight(""){case ((l, c), s) => s"\nlevel $l in line with column $c" + s}
    }
}
object IndentationSensitive
{
    import State._
    import SourcePos.sourceColumn
    import ParsecE.fail
    import Combinator.many1
    
    /**`inLine` ensures that the current position of the parser is *lined up* with the previous 
     * line's level of indentation. That is to say, it is exactly the same column number as it
     * needed to consume whitespace.
     * 
     * If this parser fails due to unaligned columns, then the error message returned will attempt
     * to suggest potential alignments that would have satisfied this parser in some way. That 
     * might involve higher levels or lower levels of indentation.*/
    final def inLine[S <: Stream[_, _], U, E]: ParsecE[S, U, E with IndentationSensitive, Unit] = 
    {
        getPosition <#> sourceColumn >>=
        (actualLevel => getRequiredIndent[S, U, E] >>=
        (requiredLevel =>
            if (actualLevel == requiredLevel) 
                popLevelCache[S, U, E]
            else 
                getParserState >>= 
                (state => fail(s"indentation doesn't match... potential levels include; ${state.prettyLevelCache(actualLevel)}"))))
    }
    
    /**Executes the parser `p` after first setting the current required whitespace to remain on this
     * level to the current column of the parser. Once `p` has finished consuming input, the old 
     * whitespace requirement is placed back into the state. Returns the result of running parser `p`.*/
    final def withCurrentLevel[S <: Stream[_, _], U, E, A](p: ParsecE[S, U, E with IndentationSensitive, A]) =
    {
        for (oldLevel <- getRequiredIndent[S, U, E];
             pos <- getPosition;
             newLevel = pos.column;
             _ <- setRequiredIndent[S, U, E](newLevel);
             result <- pushLevelCache *> p;
             _ <- setRequiredIndent[S, U, E](oldLevel))
        yield result
    }
    
    /**This parser ensures that at the current position of the parser in the input, the column number
     * is greater than the required whitespace to remain at the current indentation level. It will 
     * fail if this isn't the case. If it is the case then you can assume the current position is 
     * either on the same level or is another level deeper than the previous level.*/
    final def ensureIndented[S <: Stream[_, _], U, E]: ParsecE[S, U, E with IndentationSensitive, Unit] =
    {
        getPosition <#> sourceColumn >>=
        (actualLevel => getRequiredIndent[S, U, E] >>=
        (requiredLevel =>
            if (actualLevel <= requiredLevel)
                fail(s"line should be indented past column $requiredLevel")
            else ()))
    }
    
    /**Given a parser `p` and a whitespace consuming parser `ws`, which *should* consume newlines, 
     * parses a block of input. This is done as follows
     * 
     * 1. Consume some whitespace with `ws`
     * 2. Ensure we are currently indented past the required level
     * 3. Using our current column number as the indentation reference run `p` *one* or more times
     *    followed by some more whitespace with `ws` ensuring that all `p`s are aligned with `inLine`.
     * 4. Replace old reference level as per `withCurrentLevel`.
     * 
     * Returns a list of results gathered from the successive applications of `p`. This parser will
     * fail if there were no lines which matches `p`.*/
    final def block[S <: Stream[_, _], U, E, A](p: ParsecE[S, U, E with IndentationSensitive, A], ws: ParsecE[S, U, E with IndentationSensitive, Unit]) =
    {
        ws *> ensureIndented *> withCurrentLevel[S, U, E, List[A]](many1(inLine *> p <* ws))
    }
    
    /**Increases the level of indentation by one level.*/
    final def incLevel[S <: Stream[_, _], U, E] = getLevel[S, U, E] >>= (l => setLevel(l+1))
    /**Decreases the level of indentation by one level.*/
    final def decLevel[S <: Stream[_, _], U, E] = getLevel[S, U, E] >>= (l => setLevel(l-1))
    
    private final def pushLevelCache[S <: Stream[_, _], U, E] = 
    {
        updateParserState[S, U, E with IndentationSensitive](s =>
        {
            val s_ = s.build(Some(s), s.stateInput, s.statePos, s.stateUser)
            s_.levelCache ::= (s_.stateLevel, s_.stateRequiredIndent)
            s_
        })
    }
    
    private final def popLevelCache[S <: Stream[_, _], U, E] = 
    {
        updateParserState[S, U, E with IndentationSensitive](s =>
        {
            val s_ = s.build(Some(s), s.stateInput, s.statePos, s.stateUser)
            s_.levelCache = s_.levelCache.dropWhile{case (l, _) => l > s_.stateLevel}
            s_
        })
    }
    
    /**Returns the current level of indentation the parser is currently on. This corresponds to the
     * number of times something has been indented, **not** the amount of whitespace the level
     * represents. For the amount of whitespace for the level, see `getRequiredIndent`.*/
    final def getLevel[S <: Stream[_, _], U, E]: ParsecE[S, U, E with IndentationSensitive, Int] =
    {
        for (state <- getParserState) yield state.stateLevel
    }
    
    /**Sets the current level of indentation the parser is currently on to `level`. As with `getLevel`,
     * this corresponds to the abstract levels and not the whitespace itself.*/
    final def setLevel[S <: Stream[_, _], U, E](level: Int): ParsecE[S, U, E with IndentationSensitive, Unit] =
    {
        updateParserState[S, U, E with IndentationSensitive](s => 
        {
            val s_ = s.build(Some(s), s.stateInput, s.statePos, s.stateUser)
            s_.stateLevel = level
            s_ 
        })
    }
    
    /**Returns the amount of whitespace characters needed to remain on the same level of indentation.*/
    final def getRequiredIndent[S <: Stream[_, _], U, E]: ParsecE[S, U, E with IndentationSensitive, Int] =
    {
        for (state <- getParserState) yield state.stateRequiredIndent
    }
    
    /**Sets the amount of whitespace characters needed to remain on the same level of indentation to
     * `level`.*/
    final def setRequiredIndent[S <: Stream[_, _], U, E](level: Int): ParsecE[S, U, E with IndentationSensitive, Unit] =
    {
        updateParserState[S, U, E with IndentationSensitive](s => 
        {
            val s_ = s.build(Some(s), s.stateInput, s.statePos, s.stateUser)
            s_.stateRequiredIndent = level
            s_ 
        })
    }
    
    /**This is a `StateBuilder`, which given an optional old state and new parameters, will construct
     * a new state of the same type, which additionally copies over the fields necessary for an 
     * indentation sensitive parser. If there was no old state, the fields will be initialised to 
     * their default values.*/
    def build[S <: Stream[_, _], U](s: Option[State[S, U, IndentationSensitive] with IndentationSensitive], ts: S, pos: SourcePos, u: U): State[S, U, IndentationSensitive] with IndentationSensitive =
    {
        val s_ = new State[S, U, IndentationSensitive](ts, pos, u, build) with IndentationSensitive
        s match
        {
            case Some(s) => 
                s_.stateLevel = s.stateLevel
                s_.stateRequiredIndent = s.stateRequiredIndent
                s_.levelCache = s.levelCache
            case None => 
                s_.stateLevel = 0
                s_.stateRequiredIndent = 1
                s_.levelCache = List((0, 1))
        }
        s_
    }
}