package parsec

import scala.math.Ordering

sealed trait Message
{
    def messageString(): String
    def toEnum(): Int
    final def ==(that: Message) = toEnum == that.toEnum
    final def !=(that: Message) = toEnum != that.toEnum
    final def compare(that: Message) = if (toEnum < that.toEnum) -1 else if (toEnum > that.toEnum) 1 else 0
    final def <(that: Message) = toEnum < that.toEnum
}
case class SysUnExpect(msg: String) extends Message { final def messageString(): String = msg; final def toEnum(): Int = 0 }
case class UnExpect(msg: String) extends Message { final def messageString(): String = msg; final def toEnum(): Int = 1 }
case class Expect(msg: String) extends Message { final def messageString(): String = msg; final def toEnum(): Int = 2 }
case class RawMessage(msg: String) extends Message { final def messageString(): String = msg; final def toEnum(): Int = 3 }

case class ParseError(pos: SourcePos, msgs: List[Message])
{
    final def ++(e2: ParseError) =
    {
        val ParseError(pos2, msgs2) = e2
        if (msgs2.isEmpty && !msgs.isEmpty) this
        else if (msgs.isEmpty && !msgs2.isEmpty) e2
        else pos compare pos2 match 
        {
            case 0 => ParseError(pos, msgs ++ msgs2)
            case 1 => this
            case -1 => e2
        }
    }
    
    final def errorIsUnknown(): Boolean = msgs.isEmpty
    final def errorMessages(): List[Message] = msgs.sortWith(_ < _)
    final def +(msg: Message) = ParseError(pos, msgs :+ msg)
    final def setErrorPos(pos: SourcePos) = ParseError(pos, msgs)
    final def setErrorMessage(msg: Message) = ParseError(pos, msg :: (msgs filter (_ != msg)))
    
    final def ==(that: ParseError): Boolean = 
    {
        val messageStrs: ParseError => List[String] = _.errorMessages().map(_.messageString)
        pos == that.pos && messageStrs(this) == messageStrs(that)
    }
    
    final override def toString() = 
    {
        def showErrorMessages(msgOr: String, msgUnknown: String, msgExpecting: String, msgUnExpected: String, msgEndOfInput: String, msgs: List[Message]): String = 
        {
            @inline
            def clean(msgs: List[String]) = msgs.filter(!_.isEmpty).distinct
            @inline
            def commaSep(ms: List[String]) = clean(ms).mkString(", ")
            def commasOr(ms: List[String]) = ms match
            {
                case List() => ""
                case List(m) => m
                case ms => s"${commaSep(ms.init)} $msgOr ${ms.last}"
            }
            val showMany: (String, List[Message]) => String = (pre, msgs) => clean(msgs map (_.messageString)) match
            {
                case List() => ""
                case ms => if (pre.isEmpty) commasOr(ms) else pre + " " + commasOr(ms)     
            }
            val (sysUnExpect, msgs1) = msgs span (SysUnExpect("") == _)
            val (unExpect, msgs2) = msgs1 span (UnExpect("") == _)
            val (expect, messages) = msgs2 span (Expect("") == _)
            val showExpect = showMany(msgExpecting, expect)
            val showUnExpect = showMany(msgUnExpected, unExpect)
            val showSysUnExpect = 
            {
                lazy val firstMsg = sysUnExpect.head.messageString
                if (!unExpect.isEmpty || sysUnExpect.isEmpty) ""
                else if (firstMsg.isEmpty) msgUnExpected + " " + msgEndOfInput
                else msgUnExpected + " " + firstMsg
            }
            val showMessages = showMany("", messages)
            if (msgs.isEmpty) msgUnknown
            else clean(List(showSysUnExpect, showUnExpect, showExpect, showMessages)).map("\n"+_).mkString
        }
        pos.toString() + ":" + showErrorMessages("or", "unknown parse error", "expecting", "unexpected", s"end of ${ParseError.inputName}", errorMessages)
    }
}

case object ParseError
{
    var inputName: String = "input"
    final def unknownError[S <: Stream[_, _], U, E](s: State[S, U, E] with E) = ParseError(s.statePos, List())
    final def newErrorUnknown(pos: SourcePos) = ParseError(pos, List())
    final def newErrorMessage(msg: Message, pos: SourcePos) = ParseError(pos, List(msg))
}