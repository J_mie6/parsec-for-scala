package parsec

import Prim.{skipMany, tokenPrim, tokens}

object Char 
{
    /**`oneOf(cs)` succeeds if the current character is in the supplied list of characters `cs`.
     * Returns the parsed character. See also `satisfy`.*/
    def oneOf[S, U, E](cs: List[Char]) = satisfy[S, U, E](cs.contains(_))
    
    /**As the dual of `oneOf`, `noneOf(cs)` succeeds if the current character is not in the supplied
     * list of characters `cs`. Returns the parsed character.*/
    def noneOf[S, U, E](cs: List[Char]) = satisfy[S, U, E](!cs.contains(_))
    
    /**Skips zero or more whitespace characters. See also `skipMany`. Uses space.*/
    def spaces[S, U, E] = skipMany(space[S, U, E])
    
    /**Parses a whitespace character (either ' ' or '\t'). Returns the parsed character.*/
    def space[S, U, E]: ParsecE[Stream[S, Char], U, E, Char] = oneOf(List(' ', '\t')) ? "space/tab"
    
    /**Skips zero or more whitespace characters. See also `skipMany`. Uses whitespace.*/
    def whitespaces[S, U, E] = skipMany(whitespace[S, U, E])
    
    /**Parses a whitespace character (' ', '\t', '\n', '\r', '\f', '\v'). Returns the parsed character.*/
    def whitespace[S, U, E]: ParsecE[Stream[S, Char], U, E, Char] = oneOf(List(' ', '\t', '\n', '\r', '\f', '\u000b')) ? "whitespace"
    
    /**Parses a newline character ('\n'). Returns a newline character.*/
    def newline[S, U, E] = char[S, U, E]('\n') ? "newline"
    
    /**Parses a carriage return character '\r' followed by a newline character '\n', returns the newline character.*/
    def crlf[S, U, E] = (char[S, U, E]('\r') *> char[S, U, E]('\n')) ? "crlf newline"
    
    /**Parses a CRLF or LF end-of-line. Returns a newline character ('\n').*/
    def endOfLine[S, U, E] = (newline <|> crlf) ? "end of line"
    
    /**Parses a tab character ('\t'). Returns a tab character.*/
    def tab[S, U, E] = char[S, U, E]('\t') ? "tab"
    
    /**Parses an upper case letter. Returns the parsed character.*/
    def upper[S, U, E] = satisfy[S, U, E](_.isUpper) ? "uppercase letter"
    
    /**Parses a lower case letter. Returns the parsed character.*/
    def lower[S, U, E] = satisfy[S, U, E](_.isLower) ? "lowercase letter"
    
    /**Parses a letter or digit. Returns the parsed character.*/
    def alphaNum[S, U, E] = satisfy[S, U, E](_.isLetterOrDigit) ? "alpha-numeric character"
    
    /**Parses a letter. Returns the parsed character.*/
    def letter[S, U, E] = satisfy[S, U, E](_.isLetter) ? "letter"
    
    /**Parses a digit. Returns the parsed character.*/
    def digit[S, U, E] = satisfy[S, U, E](_.isDigit) ? "digit"
    
    /**Parses a hexadecimal digit. Returns the parsed character.*/
    def hexDigit[S, U, E] = (oneOf(('a' to 'f').toList ::: ('A' to 'F').toList) <|> digit[S, U, E]) ? "hexadecimal digit"
    
    /**Parses an octal digit. Returns the parsed character.*/
    def octDigit[S, U, E] = oneOf[S, U, E](('0' to '7').toList) ? "octal digit"
    
    /**`char(c)` parses a single character `c`. Returns the parsed character.*/
    def char[S, U, E](c: Char) = satisfy[S, U, E](_==c) ? c.toString
    
    /**This parser succeeds for any character. Returns the parsed character.*/
    def anyChar[S, U, E] = satisfy[S, U, E](_ => true) ? "any character"
    
    /**The parser `satisfy(f)` succeeds for any character for which the supplied function `f` returns
     * true. Returns the character that is actually parsed.*/
    @inline
    def satisfy[S, U, E](f: Char => Boolean): ParsecE[Stream[S, Char], U, E, Char] = 
        tokenPrim[S, Char, U, E, Char]("\"" + _.toString + "\"", pos => c => _ => pos.updatePosChar(c), c => if (f(c)) Some(c) else None)
        
    /**`string(s)` parses a sequence of characters given by `s`. Returns the parsed string.*/
    def string[S, U, E](s: String) = tokens[S, Char, U, E](_.mkString, _.updatePosString, s.toList) <#> (_.mkString)
}