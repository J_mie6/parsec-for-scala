package parsec

case class SourcePos(name: String, line: Int, column: Int)
{
    final def compare(pos2: SourcePos): Int = 
    {
        val SourcePos(_, line2, column2) = pos2
        if (line > line2) 1 else if (line2 > line) -1 else 
        if (column > column2) 1 else if (column2 > column) -1 else 0
    }
    final def incSourceLine(n: Int) = SourcePos(name, line + n, column)
    final def incSourceColumn(n: Int) = SourcePos(name, line, column + n)
    final def setSourceName(name: String) = SourcePos(name, line, column)
    final def setSourceLine(line: Int) = SourcePos(name, line, column)
    final def setSourceColumn(column: Int) = SourcePos(name, line, column)
    final def updatePosChar(c: Char) = c match 
    {
        case '\n' => SourcePos(name, line+1, 1)
        case '\t' => SourcePos(name, line, column + 8 - ((column-1) % 8))
        case _ => SourcePos(name, line, column + 1)
    }
    final def updatePosString(s: List[Char]) = s.foldLeft(this)((pos, c) => pos.updatePosChar(c))
    
    final override def toString() = 
    {
        val showLineColumn = s"(line ${line}, column ${column})"
        if (name.isEmpty()) showLineColumn
        else s"""\"$name\" $showLineColumn"""
    }
}

case object SourcePos
{
    final def initialPos(name: String) = SourcePos(name, 1, 1)
    final val sourceName: SourcePos => String = { case SourcePos(name, _, _) => name }
    final val sourceLine: SourcePos => Int = { case SourcePos(_, line, _) => line }
    final val sourceColumn: SourcePos => Int = { case SourcePos(_, _, column) => column }
    final val incSourceLine: SourcePos => Int => SourcePos = { case SourcePos(name, line, column) => n => SourcePos(name, line + n, column) }
    final val incSourceColumn: SourcePos => Int => SourcePos = { case SourcePos(name, line, column) => n => SourcePos(name, line, column + n) }
}