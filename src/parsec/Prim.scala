package parsec

import annotation.tailrec
import ParseError._

object Prim
{
    /**The parser `tokenPrim(showTok, nextPos, testTok)` accetps a token `t` with result `x` when the
     * function `testTok(t)` returns `Some(x)`. The token can be shown using `showTok(t)`. The position
     * of the *next* token should be returned when `nextPos` is called with the current source position
     * `pos`, the current token `t` and the rest of the tokens `toks`.*/
    def tokenPrim[S, T, U, E, A](showToken: =>T => String, 
                                 nextpos: =>SourcePos => T => Stream[S, T] => SourcePos, 
                                 test: =>T => Option[A]): ParsecE[Stream[S, T], U, E, A] = tokenPrimEx(showToken, nextpos, None, test)
    @inline
    def tokenPrimEx[S, T, U, E, A](showToken: T => String,
                                   nextpos: SourcePos => T => Stream[S, T] => SourcePos,
                                   optionNextState: =>Option[SourcePos => T => Stream[S, T] => U => U],
                                   test: T => Option[A]) =
    {
        new ParsecE[Stream[S, T], U, E, A](s => cok => _ => eok => eerr =>
        {        
            val State(input, pos, user, build) = s
            input.uncons() match
            {
                case None => eerr(newErrorMessage(SysUnExpect(""), pos))
                case Some((c, cs)) => test(c) match
                {
                    case Some(x) => optionNextState match
                    {
                        case None =>
                            lazy val newpos = nextpos(pos)(c)(cs)
                            lazy val newstate = build(Some(s), cs, newpos, user)
                            cok(x)(newstate)(newErrorUnknown(newpos))
                        case Some(nextState) =>
                            lazy val newpos = nextpos(pos)(c)(cs)
                            lazy val newUser = nextState(pos)(c)(cs)(user)
                            lazy val newstate = build(Some(s), cs, newpos, newUser)
                            cok(x)(newstate)(newErrorUnknown(newpos))
                    }
                    case None => eerr(newErrorMessage(SysUnExpect(showToken(c)), pos))
                }
            }
        })
    }
    
    /**`many(p)` applies the parser `p` *zero* or more times. Returns a list of the returned values of `p`.*/
    def many[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A]): ParsecE[S, U, E, List[A]] = for (xs <- manyAccum[S, U, E, A](a => as => a::as, p)) yield xs.reverse
    /**`skipMany(p)` applies the parser `p` *zero* or more times. skipping its result each time.*/
    def skipMany[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A]): ParsecE[S, U, E, Unit] = manyAccum[S, U, E, A](_ => _ => Nil, p)
    def manyAccum[S <: Stream[_, _], U, E, A](acc: A => List[A] => List[A], p_ : =>ParsecE[S, U, E, A]) = 
    {
        lazy val p = p_
        new ParsecE[S, U, E, List[A]](s => cok => cerr => eok => _ =>
        {
            val manyErr: A => State[S, U, E] with E => ParseError => Result[S, U, E] = 
                _ => _ => _ => throw new Exception("Combinator 'many' is applied to a parser that accepts an empty string")
            def walk(xs: List[A])(x: A)(s_ : State[S, U, E] with E)(err: ParseError): Result[S, U, E] =
            {
                Thunk(() => p.unParser(s_)(walk(acc(x)(xs)))(cerr)(manyErr)(e => cok(acc(x)(xs))(s_)(e)))
            }
            Thunk(() => p.unParser(s)(walk(Nil))(cerr)(manyErr)(e => eok(Nil)(s)(e)))
        })
    }
    
    def tokens[S, T, U, E](showTokens: List[T] => String, 
                           nextpos: SourcePos => List[T] => SourcePos, 
                           tts: =>List[T]): ParsecE[Stream[S, T], U, E, List[T]] = tts match
    {
        case List() => new ParsecE(s => _ => _ => eok => _ => eok(List())(s)(newErrorUnknown(s.statePos)))
        case tok::toks => 
            new ParsecE(s => cok => cerr => eok => eerr => 
            {
                lazy val State(input, pos, u, build) = s
                val errEof: ParseError = newErrorMessage(SysUnExpect(""), pos).setErrorMessage(Expect(showTokens(tts)))
                val errExpect: T => ParseError = x => newErrorMessage(SysUnExpect(showTokens(List(x))), pos).setErrorMessage(Expect(showTokens(tts)))
                val ok: Stream[S, T] => Result[Stream[S, T], U, E] = rs =>
                {
                    val pos_ = nextpos(pos)(tts)
                    val s_ = build(Some(s), rs, pos_, u)
                    cok(tts)(s_)(newErrorUnknown(pos_))
                }
                @tailrec
                def walk(tts: List[T], rs: Stream[S, T]): Result[Stream[S, T], U, E] = tts match
                {
                    case List() => ok(rs)
                    case t::ts => rs.uncons() match
                    {
                        case None => cerr(errEof)
                        case Some((x, xs)) => if (t == x) walk(ts, xs) else cerr(errExpect(x))
                    }
                }
                input.uncons() match
                {
                    case None => eerr(errEof)
                    case Some((x, xs)) => if (tok == x) walk(toks, xs) else eerr(errExpect(x))
                }
            })
    }
    
    /**`lookAhead(p)` parses `p` without consuming any input. However, if `p` fails after consuming 
     * input, so does `lookAhead`. Combine with `tryParse` if this behaviour is undesirable.*/
    def lookAhead[S <: Stream[_, _], U, E, A](p: ParsecE[S, U, E, A]) = new ParsecE[S, U, E, A](s => _ => cerr => eok => eerr =>
    {
        val eok_ : A => State[S, U, E] with E => ParseError => Result[S, U, E] = a => _ => _ => eok(a)(s)(newErrorUnknown(s.statePos))
        Thunk(() => p.unParser(s)(eok_)(cerr)(eok_)(eerr))
    })
    
    /**The parser `unexpected(msg)` always fails with an unexpected error message `msg` without consuming input.*/
    def unexpected[S <: Stream[_, _], U, E, A](msg: String): ParsecE[S, U, E, A] = new ParsecE[S, U, E, A](s => _ => _ => _ => eerr => eerr(newErrorMessage(UnExpect(msg), s.statePos)))
}