package parsec

import ParsecE.{fail, lift2, pure, tryParse}
import Expr._
import Combinator.{chainl1, chainr1, choice, many1}
import parsec.Prim.many

sealed trait Assoc
case object AssocLeft extends Assoc
case object AssocRight extends Assoc
case object AssocNone extends Assoc

sealed trait OperatorList[S <: Stream[_, _], U, E, A]
case class Infixes[S <: Stream[_, _], U, E, A](op: List[ParsecE[S, U, E, A=>A=>A]], assoc: Assoc) extends OperatorList[S, U, E, A]
case class Prefixes[S <: Stream[_, _], U, E, A](op: List[ParsecE[S, U, E, A=>A]]) extends OperatorList[S, U, E, A]
case class Postfixes[S <: Stream[_, _], U, E, A](op: List[ParsecE[S, U, E, A=>A]]) extends OperatorList[S, U, E, A]

// Old compatibility classes
sealed trait Operator[S <: Stream[_, _], U, E, A]
case class Infix[S <: Stream[_, _], U, E, A](op: ParsecE[S, U, E, A=>A=>A], assoc: Assoc) extends Operator[S, U, E, A]
case class Prefix[S <: Stream[_, _], U, E, A](op: ParsecE[S, U, E, A=>A]) extends Operator[S, U, E, A]
case class Postfix[S <: Stream[_, _], U, E, A](op: ParsecE[S, U, E, A=>A]) extends Operator[S, U, E, A]

class ExpressionParser[S <: Stream[_, _], U, E, A](table_ : =>OperatorTable[S, U, E, A], atom_ : =>ParsecE[S, U, E, A])
{
    private lazy val table = table_
    private lazy val atom = atom_
    type PrecedenceList[S <: Stream[_, _], U, E, A] = List[ParsecE[S, U, E, A]=>ParsecE[S, U, E, A]]
    
    def chainPost(p_ : =>ParsecE[S, U, E, A], op_ : =>ParsecE[S, U, E, A=>A]) = 
    {
        lazy val p = p_
        lazy val op = op_
        lift2((x: A) => (xs: List[A=>A]) => xs.foldLeft(x)((y, f) => f(y)), p, many(op))
    }
    def chainPre(p_ : =>ParsecE[S, U, E, A], op_ : =>ParsecE[S, U, E, A=>A]) = 
    {
        lazy val p = p_
        lazy val op = op_
        lift2((xs: List[A=>A]) => (x: A) => xs.foldRight(x)((f, y) => f(y)), many(op), p)
    }
    
    private def convertOperator(ops_ : =>OperatorList[S, U, E, A]): ParsecE[S, U, E, A]=>ParsecE[S, U, E, A] = { lazy val ops = ops_; ops match
    {
        case Infixes(ops, assoc) => assoc match
        {
            case AssocLeft  => chainl1(_, choice(ops))
            case AssocRight => chainr1(_, choice(ops))
            case AssocNone => throw new UnsupportedOperationException("NoAss is not supported by new expression parser")
        }
        case Prefixes(ops) => chainPre(_, choice(ops))
        case Postfixes(ops) => chainPost(_, choice(ops))
    }}
    
    private def convertTable(table: =>OperatorTable[S, U, E, A]) = table map (convertOperator(_))
    private def flatten(list: =>PrecedenceList[S, U, E, A]) = list.foldLeft(atom)((p, op) => op(p))
    
    lazy val expressionParser = flatten(convertTable(table))
}

object Expr 
{
    type OperatorTable[S <: Stream[_, _], U, E, A] = List[OperatorList[S, U, E, A]]
    type OperatorTable_[S <: Stream[_, _], U, E, A] = List[List[Operator[S, U, E, A]]]
    
    def buildExpressionParser[S <: Stream[_, _], U, E, A](operators: OperatorTable_[S, U, E, A], simpleExpr: ParsecE[S, U, E, A]) = 
    {
        operators.foldLeft(simpleExpr)(makeParser)
        def makeParser(term: ParsecE[S, U, E, A], ops: List[Operator[S, U, E, A]]): ParsecE[S, U, E, A] =
        {
            type PListB = List[ParsecE[S, U, E, A => A => A]]
            type PListU = List[ParsecE[S, U, E, A => A]]
            val splitOp: ((PListB, PListB, PListB, PListU, PListU), 
                          Operator[S, U, E, A]) => 
                         (PListB, PListB, PListB, PListU, PListU) = 
            { 
                case ((rassoc, lassoc, nassoc, prefix, postfix), Infix(op, assoc)) => assoc match
                {
                    case AssocNone => (rassoc, lassoc, nassoc :+ op, prefix, postfix)
                    case AssocLeft => (rassoc, lassoc :+ op, nassoc, prefix, postfix)
                    case AssocRight => (rassoc :+ op, lassoc, nassoc, prefix, postfix)
                }
                case ((rassoc, lassoc, nassoc, prefix, postfix), Prefix(op)) => (rassoc, lassoc, nassoc, prefix :+ op, postfix)
                case ((rassoc, lassoc, nassoc, prefix, postfix), Postfix(op)) => (rassoc, lassoc, nassoc, prefix, postfix :+ op)
            }
            val (rassoc, lassoc, nassoc, prefix, postfix) = ops.foldLeft(List(): PListB, List(): PListB, List(): PListB, List(): PListU, List(): PListU)(splitOp)
            val rassocOp = choice(rassoc)
            val lassocOp = choice(lassoc)
            val nassocOp = choice(nassoc)
            val prefixOp = choice(prefix) ? ""
            val postfixOp = choice(postfix) ? ""
            def ambiguous(assoc: String, op: ParsecE[S, U, E, A=>A=>A]) =
            {
                tryParse(op >> fail[S, U, E, A](s"ambiguous use of a $assoc associative operator"))
            }
            val ambiguousRight = ambiguous("right", rassocOp)
            val ambiguousLeft = ambiguous("left", lassocOp)
            val ambiguousNone = ambiguous("non", nassocOp)
            val postfixP = postfixOp </> ((x: A)=>x)
            val prefixP = prefixOp </> ((x: A)=>x)
            val termP = for (pre <- prefixP;
                             x <- term;
                             post <- postfixP)
                        yield post(pre(x))
            
            def rassocP(x: A): ParsecE[S, U, E, A] = (for (f <- rassocOp;
                                                           z <- termP;
                                                           y <- rassocP1(z))
                                                      yield f(x)(y)) <|> ambiguousLeft <|> ambiguousNone
            def rassocP1(x: A) = rassocP(x) </> x
            
            def lassocP(x: A): ParsecE[S, U, E, A] = lassocOp .>>= (f => termP >>= (y => lassocP1(f(x)(y)))) <|> ambiguousRight <|> ambiguousNone
            def lassocP1(x: A) = lassocP(x) </> x
            
            def nassocP(x: A) = nassocOp >>= (f => termP >>= (y => ambiguousRight <|> ambiguousLeft <|> ambiguousNone </> f(x)(y)))
            
            termP >>= (x => (rassocP(x) <|> lassocP(x) <|> nassocP(x) </> x) ? "operator")
        }
    }
}