package parsec

import parsec.ParsecE._
import parsec.Prim.{many, skipMany, tokenPrim, unexpected}

object Combinator
{
    /**`choice(ps)` tries to apply the parsers in the list `ps` in order, until one of them succeeds.
     *  Returns the value of the succeeding parser.*/
    def choice[S <: Stream[_, _], U, E, A](ps: =>List[ParsecE[S, U, E, A]]): ParsecE[S, U, E, A] = ps.reduceLeftOption(_<|>_).getOrElse(empty[S, U, E, A])
    
    /**`trychoice(ps)` tries to apply the parsers in the list `ps` in order, until one of them succeeds.
     *  Returns the value of the succeeding parser. Utilises <\> vs choice's <|>.*/
    def tryChoice[S <: Stream[_, _], U, E, A](ps: =>List[ParsecE[S, U, E, A]]): ParsecE[S, U, E, A] = ps.reduceLeftOption(_<\>_).getOrElse(empty[S, U, E, A])
    
    /** `count(n, p)` parses `n` occurrences of `p`. If `n` is smaller or equal to zero, the parser is
     *  `pure(List())`. Returns a list of `n` values returned by `p`.*/
    def count[S <: Stream[_, _], U, E, A](n: Int, p: ParsecE[S, U, E, A]): ParsecE[S, U, E, List[A]] = sequence(for (_ <- 1 to n) yield p)
    
    /**`option(x, p)` tries to apply parser `p`. If `p` fails without consuming input, it returns the
     *  value `x`, otherwise the value returned by `p`.*/
    def option[S <: Stream[_, _], U, E, A](x: =>A, p: =>ParsecE[S, U, E, A]): ParsecE[S, U, E, A] = p </> x
    
    /**`optionMaybe(p)` tries to apply parser `p`. If `p` fails without consuming input, it returns 
     * `None`, otherwise it returns `Some` of the value returned by `p`.*/
    def optionMaybe[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A]): ParsecE[S, U, E, Option[A]] = option(None, p <#> (Some(_)))
    
    /**`decide(p)` removes the option from inside parser `p`, and if it returned `None` will fail.**/
    // TODO for (opt <- p; if opt.isDefined) yield opt.get
    def decide[S <: Stream[_, _], U, E, A](p: ParsecE[S, U, E, Option[A]]): ParsecE[S, U, E, A] = p >>= {case Some(x) => pure(x); case None => empty[S, U, E, A]}
    
    /**`optional(p)` tries to apply parser `p`. It will parse `p` or nothing. It only fails if `p` 
     * fails after consuming input. It discards the result of `p`.*/
    // CODO This could be a Nothing parser, with an instrinsic-esque approach that code gens {InputCheck; p; Pop; JumpGood} with no actual branch
    // TODO The above would obsoleted by the peephole optimisation of; p <* (q <|> pure _) => p; InputCheck; q; Pop; JumpGood
    //                                                                 (q <|> pure _) *> p => InputCheck; q; Pop; JumpGood; p
    def optional[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A]): ParsecE[S, U, E, Unit] = p.unit() </> Unit
    
    /**`between(open, close, p)` parses `open`, followed by `p` and `close`. Returns the value returned by `p`.*/
    def between[S <: Stream[_, _], U, E, A, O, C](open: =>ParsecE[S, U, E, O],
                                                  close: =>ParsecE[S, U, E, C],
                                                  p: =>ParsecE[S, U, E, A]): ParsecE[S, U, E, A] = open *> p <* close
        
    /**`skipMany1(p)` applies the parser `p` *one* or more times, skipping its result.*/
    def skipMany1[S <: Stream[_, _], U, E, A](p_ : =>ParsecE[S, U, E, A]): ParsecE[S, U, E, Unit] =
    {
        lazy val p = p_
        p *> skipMany(p)
    }
        
    /**`many1(p)` applies the parser `p` *one* or more times. Returns a list of the returned values of `p`.*/
    def many1[S <: Stream[_, _], U, E, A](p_ : =>ParsecE[S, U, E, A]): ParsecE[S, U, E, List[A]] =
    {
        lazy val p = p_
        p <::> many(p)
    }
    
    /**`sepBy(p, sep)` parses *zero* or more occurrences of `p`, separated by `sep`. Returns a list 
     * of values returned by `p`.*/
    def sepBy[S <: Stream[_, _], U, E, A, B](p: =>ParsecE[S, U, E, A], sep: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, List[A]] = sepBy1(p, sep) </> Nil
    
    /**`sepBy1(p, sep)` parses *one* or more occurrences of `p`, separated by `sep`. Returns a list
     *  of values returned by `p`.*/
    def sepBy1[S <: Stream[_, _], U, E, A, B](p_ : =>ParsecE[S, U, E, A], sep_ : =>ParsecE[S, U, E, B]): ParsecE[S, U, E, List[A]] =
    {
        lazy val p = p_
        lazy val sep = sep_
        p <::> many(sep *> p)
    }
                                                                                       
    /**`sepEndBy1(p, sep)` parses *one* or more occurrences of `p`, separated and optionally ended 
     * by `sep`. Returns a list of values returned by `p`.*/
    def sepEndBy1[S <: Stream[_, _], U, E, A, B](p: =>ParsecE[S, U, E, A], _sep: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, List[A]] =
    {
       lazy val sep = _sep
       sepBy1(p, sep) <* optional(sep)
    }
    
    /**`sepEndBy(p, sep)` parses *zero* or more occurrences of `p`, separated and optionally ended 
     * by `sep`. Returns a list of values returned by `p`.*/
    def sepEndBy[S <: Stream[_, _], U, E, A, B](p: =>ParsecE[S, U, E, A], sep: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, List[A]] = sepEndBy1(p, sep) </> Nil
    
    /**`endBy1(p, sep)` parses *one* or more occurrences of `p`, separated by `sep`. Returns a list
     * of values returned by `p`.*/
    def endBy1[S <: Stream[_, _], U, E, A, B](p: =>ParsecE[S, U, E, A], sep: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, List[A]] = many1(p <* sep)
    
    /**`endBy(p, sep)` parses *zero* or more occurrences of `p`, separated by `sep`. Returns a list
     * of values returned by `p`.*/
    def endBy[S <: Stream[_, _], U, E, A, B](p: =>ParsecE[S, U, E, A], sep: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, List[A]] = many(p <* sep)
    
    /**`chainr(p, op, x)` parses *zero* or more occurrences of `p`, separated by `op`. Returns a value
     * obtained by a right associative application of all functions return by `op` to the values 
     * returned by `p`. If there are no occurrences of `p`, the value `x` is returned.*/
    def chainr[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A], op: =>ParsecE[S, U, E, A => A => A], x: =>A): ParsecE[S, U, E, A] = chainr1(p, op) </> x
    
    /**`chainl(p, op, x)` parses *zero* or more occurrences of `p`, separated by `op`. Returns a value
     * obtained by a left associative application of all functions returned by `op` to the values
     * returned by `p`. If there are no occurrences of `p`, the value `x` is returned.*/
    def chainl[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A], op: =>ParsecE[S, U, E, A => A => A], x: =>A): ParsecE[S, U, E, A] = chainl1(p, op) </> x
    
    /**`chainr1(p, op)` parses *one* or more occurrences of `p`, separated by `op`. Returns a value
     * obtained by a right associative application of all functions return by `op` to the values 
     * returned by `p`.*/
    def chainr1[S <: Stream[_, _], U, E, A](p_ : =>ParsecE[S, U, E, A], op_ : =>ParsecE[S, U, E, A => A => A]): ParsecE[S, U, E, A] =
    {
        lazy val p = p_
        lazy val op = op_
        lift2((xs: List[A=>A]) => (x: A) => xs.foldRight(x)((f, y) => f(y)), many(tryParse(p <**> op)), p)
    }
    
    /**`chainl1(p, op)` parses *one* or more occurrences of `p`, separated by `op`. Returns a value
     * obtained by a left associative application of all functions return by `op` to the values 
     * returned by `p`. This parser can for example be used to eliminate left recursion which
     * typically occurs in expression grammars.*/
    def chainl1[S <: Stream[_, _], U, E, A](p_ : =>ParsecE[S, U, E, A], op_ : =>ParsecE[S, U, E, A => A => A]): ParsecE[S, U, E, A] =
    {
        lazy val p = p_
        lazy val op = op_
        lift2((x: A) => (xs: List[A=>A]) => xs.foldLeft(x)((y, f) => f(y)), p, many(lift2(flip[A, A, A], op, p)))
    }
    
    /**The parser `anyToken()` accepts any kind of token. It is for example used to implement `eof`.
     * Returns the accepted token.*/
    def anyToken[S, T, U, E]: ParsecE[Stream[S, T], U, E, T] = tokenPrim(_.toString, pos => _ => _ => pos, Some(_))
    
    /**This parser only succeeds at the end of the input. This is not a primitive parser but it is
     * defined using `notFollowedBy`.*/
    def eof[S, T, U, E]: ParsecE[Stream[S, T], U, E, Unit] = notFollowedBy[Stream[S, T], U, E, T](anyToken[S, T, U, E]) ? "end of input"
    
    /**`notFollowedBy(p)` only succeeds when parser `p` fails. This parser does not consume any input.
     * This parser can be used to implement the 'longest match' rule. For example, when recognising
     * keywords, we want to make sure that a keyword is not followed by a legal identifier character,
     * in which case the keyword is actually an identifier. We can program this behaviour as follows:
     * {{{tryParse(string(kw) >> notFollowedBy(alphaNum))}}}*/
    def notFollowedBy[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A]): ParsecE[S, U, E, Unit] =
    {
        //TODO The >>= can be removed here, with skillful use of fail(p, msg)
        tryParse((tryParse(p) >>= (c => unexpected[S, U, E, Unit]("\"" + c.toString + "\""))) </> Unit)
    }
    
    /**`manyTill(p, end)` applies parser `p` zero or more times until the parser `end` succeeds.
     * Returns a list of values returned by `p`. This parser can be used to scan comments.*/
    // TODO Candidate for intrinsic; manyTill(p, term@(end #> End)) => Label(0); InputCheck(1); end; Label(1); JumpGood(2); p; Label(2); ManyTill(0) { tos == End }
    def manyTill[S <: Stream[_, _], U, E, A, B](p_ : =>ParsecE[S, U, E, A], end_ : =>ParsecE[S, U, E, B]): ParsecE[S, U, E, List[A]] =
    {
        lazy val p = p_
        lazy val end = end_
        lazy val scan: ParsecE[S, U, E, List[A]] = (end #> Nil) <|> (p <::> scan)
        scan
    }
    
    /**`many1Till(p, end)` applies parser `p` one or more times until the parser `end` succeeds.
     * Returns a list of values returned by `p`.*/
    def many1Till[S <: Stream[_, _], U, E, A, B](_p: =>ParsecE[S, U, E, A], _end: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, Unit] =
    {
        lazy val p = _p
        lazy val end = _end
        notFollowedBy(end) *> (p <::> manyTill(p, end))
    }
}