package parsec

import ParsecE._
import ParseError._

final class ParsecE[S <: Stream[_, _], U, E, +A](val unParser: State[S, U, E] with E => 
                                                               (A => State[S, U, E] with E => ParseError => Result[S, U, E]) =>
                                                               (ParseError => Result[S, U, E]) =>
                                                               (A => State[S, U, E] with E => ParseError => Result[S, U, E]) =>
                                                               (ParseError => Result[S, U, E]) => Result[S, U, E]) extends AnyVal
{
    @inline
    def flatMap[B](f: =>A => ParsecE[S, U, E, B]): ParsecE[S, U, E, B] =
    {
        new ParsecE[S, U, E, B](s => cok => cerr => eok => eerr => 
        {
            val mcok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = x => s_ => err => 
            {
                Thunk(() => f(x).unParser(s_)(cok)(cerr)(x => s_ => err_ => cok(x)(s_)(err ++ err_))(err_ => cerr(err ++ err_)))
            }
            val meok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = x => s_ => err =>
            {
                Thunk(() => f(x).unParser(s_)(cok)(cerr)(x => s_ => err_ => eok(x)(s_)(err ++ err_))(err_ => eerr(err ++ err_)))
            }
            Thunk(() => unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    @inline
    def flatten[B](implicit f: A => ParsecE[S, U, E, B]): ParsecE[S, U, E, B] =
    {
        new ParsecE[S, U, E, B](s => cok => cerr => eok => eerr => 
        {
            val mcok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = ma => s_ => err => 
            {
                Thunk(() => ma.unParser(s_)(cok)(cerr)(x => s_ => err_ => cok(x)(s_)(err ++ err_))(err_ => cerr(err ++ err_)))
            }
            val meok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = ma => s_ => err =>
            {
                Thunk(() => ma.unParser(s_)(cok)(cerr)(x => s_ => err_ => eok(x)(s_)(err ++ err_))(err_ => eerr(err ++ err_)))
            }
            Thunk(() => unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    @inline
    def bind[B](f: =>A => ParsecE[S, U, E, B]): ParsecE[S, U, E, B] = flatMap(f)
    @inline
    def >>=[B](f: =>A => ParsecE[S, U, E, B]): ParsecE[S, U, E, B] = flatMap(f)
    @inline
    def >>[B](mb: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, B] =
    {
        new ParsecE[S, U, E, B](s => cok => cerr => eok => eerr => 
        {
            val mcok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = _ => s_ => err => 
            {
                Thunk(() => mb.unParser(s_)(cok)(cerr)(x => s_ => err_ => cok(x)(s_)(err ++ err_))(err_ => cerr(err ++ err_)))
            }
            val meok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = _ => s_ => err =>
            {
                Thunk(() => mb.unParser(s_)(cok)(cerr)(x => s_ => err_ => eok(x)(s_)(err ++ err_))(err_ => eerr(err ++ err_)))
            }
            Thunk(() => unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    @inline
    def <*>:[B](ff: =>ParsecE[S, U, E, A => B]): ParsecE[S, U, E, B] =
    {
        new ParsecE[S, U, E, B](s => cok => cerr => eok => eerr => 
        {
            val mcok: ((A => B) => State[S, U, E] with E => ParseError => Result[S, U, E]) = f => s_ => err => 
            {
                Thunk(() => unParser(s_)(x => cok(f(x)))(cerr)(x => s_ => err_ => cok(f(x))(s_)(err ++ err_))(err_ => cerr(err ++ err_)))
            }
            val meok: ((A => B) => State[S, U, E] with E => ParseError => Result[S, U, E]) = f => s_ => err =>
            {
                Thunk(() => unParser(s_)(x => cok(f(x)))(cerr)(x => s_ => err_ => eok(f(x))(s_)(err ++ err_))(err_ => eerr(err ++ err_)))
            }
            Thunk(() => ff.unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    @inline
    def <*>[B, C](p: =>ParsecE[S, U, E, B])(implicit g: A => (B => C)): ParsecE[S, U, E, C] =
    {
        new ParsecE[S, U, E, C](s => cok => cerr => eok => eerr => 
        {
            val mcok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = f => s_ => err => 
            {
                Thunk(() => p.unParser(s_)(x => cok(f(x)))(cerr)(x => s_ => err_ => cok(f(x))(s_)(err ++ err_))(err_ => cerr(err ++ err_)))
            }
            val meok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = f => s_ => err =>
            {
                Thunk(() => p.unParser(s_)(x => cok(f(x)))(cerr)(x => s_ => err_ => eok(f(x))(s_)(err ++ err_))(err_ => eerr(err ++ err_)))
            }
            Thunk(() => unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    @inline
    def <**>[B](f: =>ParsecE[S, U, E, A => B]): ParsecE[S, U, E, B] = lift2[S, U, E, A, A=>B, B](x => f => f(x), this, f)
    @inline
    def <*[B](mb: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, A] =
    {
        new ParsecE[S, U, E, A](s => cok => cerr => eok => eerr => 
        {
            val mcok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = x => s_ => err => 
            {
                Thunk(() => mb.unParser(s_)(_ => cok(x))(cerr)(_ => s_ => err_ => cok(x)(s_)(err ++ err_))(err_ => cerr(err ++ err_)))
            }
            val meok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = x => s_ => err =>
            {
                Thunk(() => mb.unParser(s_)(_ => cok(x))(cerr)(_ => s_ => err_ => eok(x)(s_)(err ++ err_))(err_ => eerr(err ++ err_)))
            }
            Thunk(() => unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    @inline
    def *>[B](mb: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, B] = this >> mb
    @inline
    def map[B](f: =>A => B): ParsecE[S, U, E, B] = new ParsecE[S, U, E, B](s => cok => cerr => eok => eerr => Thunk(() => unParser(s)(x => cok(f(x)))(cerr)(x => eok(f(x)))(eerr)))
    @inline
    def unit(): ParsecE[S, U, E, Unit] = new ParsecE[S, U, E, Unit](s => cok => cerr => eok => eerr => Thunk(() => unParser(s)(_ => cok(()))(cerr)(_ => eok(()))(eerr)))
    @inline
    def <#>[B](f: =>A => B): ParsecE[S, U, E, B] = map(f)
    @inline
    def <#>:[B](f: =>A => B): ParsecE[S, U, E, B] = map(f)
    @inline
    def #>[B](b: =>B): ParsecE[S, U, E, B] = new ParsecE[S, U, E, B](s => cok => cerr => eok => eerr => Thunk(() => unParser(s)(_ => cok(b))(cerr)(_ => eok(b))(eerr)))
    @inline
    def <|>[A_ >: A](n: =>ParsecE[S, U, E, A_]): ParsecE[S, U, E, A_] =
    {
        new ParsecE[S, U, E, A_](s => cok => cerr => eok => eerr =>
        {
            val meerr: ParseError => Result[S, U, E] = err =>
            {
                val neok: A_ => State[S, U, E] with E => ParseError => Result[S, U, E] = y => s_ => err_ => eok(y)(s_)(err ++ err_)
                val neerr: ParseError => Result[S, U, E] = err_ => eerr(err ++ err_)
                Thunk(() => n.unParser(s)(cok)(cerr)(neok)(neerr))
            }
            Thunk(() => unParser(s)(cok)(cerr)(eok)(meerr))
        })
    }
    @inline
    def <\>[A_ >: A](n: =>ParsecE[S, U, E, A_]): ParsecE[S, U, E, A_] =
    {
        new ParsecE[S, U, E, A_](s => cok => cerr => eok => eerr =>
        {
            val meerr: ParseError => Result[S, U, E] = err =>
            {
                val neok: A_ => State[S, U, E] with E => ParseError => Result[S, U, E] = y => s_ => err_ => eok(y)(s_)(err ++ err_)
                val neerr: ParseError => Result[S, U, E] = err_ => eerr(err ++ err_)
                Thunk(() => n.unParser(s)(cok)(cerr)(neok)(neerr))
            }
            Thunk(() => unParser(s)(cok)(meerr)(eok)(meerr))
        })
    }
    def </>[A_ >: A](x: =>A_): ParsecE[S, U, E, A_] = this <|> pure(x)
    def ?(msg: =>String): ParsecE[S, U, E, A] =
    {
        new ParsecE[S, U, E, A](s => cok => cerr => eok => eerr =>
        {
            lazy val eok_ : A => State[S, U, E] with E => ParseError => Result[S, U, E] =
                x => s_ => err => eok(x)(s_)(if (err.errorIsUnknown) err else err.setErrorMessage(Expect(msg)))
            lazy val eerr_ : ParseError => Result[S, U, E] =
                err => eerr(err.setErrorMessage(Expect(msg)))
            Thunk(() => unParser(s)(cok)(cerr)(eok_)(eerr_))
        })
    }
    def filter(p: A => Boolean): ParsecE[S, U, E, A] =
    {
        new ParsecE[S, U, E, A](s => cok => cerr => eok => eerr => 
        {
            val mcok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = x => s_ => err => 
            {
                if (p(x)) cok(x)(s_)(err)
                else eerr(err)
            }
            val meok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = x => s_ => err =>
            {
                if (p(x)) eok(x)(s_)(err)
                else eerr(err)
            }
            Thunk(() => unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    def withFilter(p: A => Boolean): ParsecE[S, U, E, A] = filter(p)
    def <::>[A_ >: A](xs: ParsecE[S, U, E, List[A_]]): ParsecE[S, U, E, List[A_]] = lift2((x: A) => (xs: List[A_]) => x::xs, this, xs)
    def <|?>[B](p: ParsecE[S, U, E, B], q: ParsecE[S, U, E, B])(implicit ev: ParsecE[S, U, E, A] => ParsecE[S, U, E, Boolean]): ParsecE[S, U, E, B] = choose(this, p, q)
    def >?>(pred: A => Boolean, msggen: A => String): ParsecE[S, U, E, A] = guard(this, pred, msggen)
    def >?>(pred: A => Boolean, msg: String): ParsecE[S, U, E, A] = guard(this, pred, msg)
}

object ParsecE
{
    @inline
    def pure[S <: Stream[_, _], U, E, A](a: =>A) = new ParsecE[S, U, E, A](s => _ => _ => eok => _ => eok(a)(s)(unknownError(s)))
    @inline
    def join[S <: Stream[_, _], U, E, A](mma: =>ParsecE[S, U, E, ParsecE[S, U, E, A]]) =
    {
        new ParsecE[S, U, E, A](s => cok => cerr => eok => eerr => 
        {
            val mcok: (ParsecE[S, U, E, A] => State[S, U, E] with E => ParseError => Result[S, U, E]) = ma => s_ => err => 
            {
                Thunk(() => ma.unParser(s_)(cok)(cerr)(x => s_ => err_ => cok(x)(s_)(err ++ err_))(err_ => cerr(err ++ err_)))
            }
            val meok: (ParsecE[S, U, E, A] => State[S, U, E] with E => ParseError => Result[S, U, E]) = ma => s_ => err =>
            {
                Thunk(() => ma.unParser(s_)(cok)(cerr)(x => s_ => err_ => eok(x)(s_)(err ++ err_))(err_ => eerr(err ++ err_)))
            }
            Thunk(() => mma.unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    @inline
    def empty[S <: Stream[_, _], U, E, A] = new ParsecE[S, U, E, A](s => _ => _ => _ => eerr => eerr(unknownError(s)))
    @inline
    def fmap[S <: Stream[_, _], U, E, A, B](f: =>A => B)(fa: ParsecE[S, U, E, A]) = fa map f
    @inline
    def ap[S <: Stream[_, _], U, E, A, B](f: =>ParsecE[S, U, E, A=>B])(fa: ParsecE[S, U, E, A]) = f <*>: fa
    @inline
    def lift2[S <: Stream[_, _], U, E, A, B, C](f: =>A => B => C, p: =>ParsecE[S, U, E, A], q: =>ParsecE[S, U, E, B]): ParsecE[S, U, E, C] =
    {
        new ParsecE[S, U, E, C](s => cok => cerr => eok => eerr => 
        {
            val mcok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = x => s_ => err => 
            {
                Thunk(() => q.unParser(s_)(y => cok(f(x)(y)))(cerr)(y => s_ => err_ => cok(f(x)(y))(s_)(err ++ err_))(err_ => cerr(err ++ err_)))
            }
            val meok: (A => State[S, U, E] with E => ParseError => Result[S, U, E]) = x => s_ => err =>
            {
                Thunk(() => q.unParser(s_)(y => cok(f(x)(y)))(cerr)(y => s_ => err_ => eok(f(x)(y))(s_)(err ++ err_))(err_ => eerr(err ++ err_)))
            }
            Thunk(() => p.unParser(s)(mcok)(cerr)(meok)(eerr))
        })
    }
    @inline
    def sequence[S <: Stream[_, _], U, E, A](ps: Seq[ParsecE[S, U, E, A]]): ParsecE[S, U, E, List[A]] =
    {
        // This should be :: and reverse :)
        ps.foldLeft(pure[S, U, E, List[A]](List()))((ys, x) => lift2((zs: List[A]) => (z: A) => zs :+ z, ys, x))
    }
    @inline
    def traverse[S <: Stream[_, _], U, E, A, B](f: A => ParsecE[S, U, E, B])(xs: Seq[A]): ParsecE[S, U, E, List[B]] = sequence(xs.map(f))
    @inline
    def fail[S <: Stream[_, _], U, E, A](msg: =>String): ParsecE[S, U, E, A] = new ParsecE[S, U, E, A](s => _ => _ => _ => eerr => eerr(newErrorMessage(RawMessage(msg), s.statePos)))
    def fail[S <: Stream[_, _], U, E, A](p: ParsecE[S, U, E, A], msg: A => String): ParsecE[S, U, E, A] = p >>= (x => fail(msg(x)))
    @inline
    def label[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A])(msg: String) = p ? msg
    @inline
    def tryParse[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A]) = new ParsecE[S, U, E, A](s => cok => _ => eok => eerr => Thunk(() => p.unParser(s)(cok)(eerr)(eok)(eerr)))
    def choose[S <: Stream[_, _], U, E, A](b: =>ParsecE[S, U, E, Boolean], p: =>ParsecE[S, U, E, A], q: =>ParsecE[S, U, E, A]): ParsecE[S, U, E, A] =
    {
        b >>= (b => if (b) p else q)
    }
    def guard[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A], pred: A => Boolean, msg: String): ParsecE[S, U, E, A] =
    {
        p >>= (x => if (pred(x)) pure(x) else fail(msg))
    }
    def guard[S <: Stream[_, _], U, E, A](p: =>ParsecE[S, U, E, A], pred: A => Boolean, msggen: A => String): ParsecE[S, U, E, A] =
    {
        p >>= (x => if (pred(x)) pure(x) else fail(pure(x), msggen))
    }
}

import Char._
import Prim.many
import Combinator.eof
import Combinator.notFollowedBy
import Combinator.anyToken
import Combinator.sepEndBy1
import Combinator.chainl1
import Combinator.chainr1
import Combinator.count
import Combinator.many1
import Prim.skipMany
import Expr.OperatorTable
object Test
{
    def main(args: Array[String]): Unit = 
    {
        println(runParser[Stream[String, Char], Unit, Unit]((), (), "", "bla"))
        val p: Parser[Int] = pure(10)
        val q: Parser[Int] = pure(20)
        println(runParser[Stream[String, Char], Unit, Int](p, (), "", "bla"))
        val r = p >>= (x => pure(x))
        println(runParser[Stream[String, Char], Unit, Int](r, (), "", "bla"))
        var s = for (x <- p; 
                     y <- q) 
                yield x + y
        println(runParser[Stream[String, Char], Unit, Int](s, (), "", "bla")) 
        println(runParser[Stream[String, Char], Unit, Int]((((x: Int) => (y: Int) => x + y) <#>: p) <*> q, (), "", "bla"))
        println(runParser[Stream[String, Char], Unit, Int](p >> q, (), "", "bla"))
        println(runParser[Stream[String, Char], Unit, Int](p <* q, (), "", "bla"))
        println(runParser[Stream[String, Char], Unit, Int](p #> 5, (), "", "bla"))
        
        val t: ParsecE[Stream[List[Int], Int], Unit, Default, Int] = pure(100)
        println(runParser[Stream[List[Int], Int], Unit, Int](t, (), "", List(1, 2, 3)))
        
        val ab: ParsecE[Stream[List[Char], Char], Unit, Default, String] = for (x <- char('a'); y <- char('b')) yield "" + x + y
        println(runParser[Stream[List[Char], Char], Unit, String](ab, (), "", List('a', 'b')))
        
        //val as: Parser[String] = (many(char[String, Unit, Default]('a'))) <#> (_.mkString)
        val as: Parser[String] = count(3, char[String, Unit, Default]('a')) <#> (_.mkString)
        println(runParser[Stream[String, Char], Unit, String](as, (), "", "aaaa"))
        
        val aorb: Parser[Char] = char('c') ? "something"// <|> pure('b') *> pure('c')
        println(runParser[Stream[String, Char], Unit, Char](aorb, (), "", "b"))
        
        val nfba: Parser[Unit] = notFollowedBy(char[String, Unit, Default]('a') ? "Hello world") 
        println(runParser[Stream[String, Char], Unit, Unit](nfba, (), "", "a"))
        
        val any: Parser[Char] = anyToken[String, Char, Unit, Default]
        println(runParser[Stream[String, Char], Unit, Char](any, (), "", "c"))
        
        val ss: Parser[String] = (string("hello world") <* char[String, Unit, Default]('!')) <* eof
        println(runParser[Stream[String, Char], Unit, String](ss, (), "", "hello world! "))
        
        val sep: Parser[List[Char]] = sepEndBy1(char('a'), char(','))
        println(runParser[Stream[String, Char], Unit, List[Char]](sep, (), "", "a,a,a,a,a,a,"))
        
        val chain: Parser[String] = chainl1(string("ab"), pure((s: String) => (t: String) => s + t))
        println(runParser[Stream[String, Char], Unit, String](chain, (), "", "abababababababab"))
        
        val lang: LanguageDef[String, Unit, BracketTracking] = LanguageDef("", "", "", false, char('$'), oneOf(List('x', 'y', 'z')), oneOf(List('+', '-', '*')), oneOf(List('+', '-', '*')),
                                                                   List("$xyz"), List("+", "++", "-", "*"), true, space)
        val tok = new BracketTracking.TokenParser(lang)
        type ParserB[A] = ParsecE[Stream[String, Char], Unit, BracketTracking, A]
        val v: ParserB[Unit] = tok.operator("+")
        val vv: ParserB[Unit] = tok.operator("++")
        val chn: ParserB[Int] = chainr1(tok.natural, tok.symbol("%") #> ((x: Int) => (y: Int) => x % y))
        println(runParser[Stream[String, Char], Unit, BracketTracking, Int](chn, (), "", "7 % 5 % 3", BracketTracking.build, 1, 1))
        println(runParser[Stream[String, Char], Unit, BracketTracking, Unit](tok.identifier >> (v<|>vv), (), "", "$x++ +", BracketTracking.build, 1, 1))
        
        var start = System.currentTimeMillis()
        val ops: OperatorTable[Stream[String, Char], Unit, BracketTracking, Int] = List(Postfixes(List(tok.brackets(tok.natural <#> (y => x => x % y)))),
                                                                                        Postfixes(List(tok.symbol_("++") #> ((x: Int) => x + 1))),
                                                                                        Prefixes(List(tok.symbol_("-") #> ((x: Int) => -x))),
                                                                                        Infixes(List(tok.operator("*") #> ((x: Int) => (y: Int) => x * y)), AssocLeft),
                                                                                        Infixes(List(tok.operator("+") #> ((x: Int) => (y: Int) => x + y),
                                                                                                     tok.operator("-") #> ((x: Int) => (y: Int) => x - y)), AssocLeft))
        lazy val atom: ParserB[Int] = tok.natural <|> tok.parens(expr)
        lazy val expr: ParserB[Int] = new ExpressionParser[Stream[String, Char], Unit, BracketTracking, Int](ops, atom).expressionParser
        //for (i <- 0 to 50000) runParser[Stream[String, Char], Unit, BracketTracking, Int](expr, (), "", "(1 + 1) + -(2 + 3 - 0) * 7", BracketTracking.build, 1, 1)
        println(System.currentTimeMillis() - start)
        println(runParser[Stream[String, Char], Unit, BracketTracking, Int](expr, (), "", "(1 + 1) + -(2 + 3 - 0) * 7", BracketTracking.build, 1, 1))
        
        val ip: ParsecE[Stream[String, Char], Unit, IndentationSensitive with BracketTracking, Int] = pure(7)
        val ip_ = for (x <- ip; _ <- IndentationSensitive.setLevel(10); l <- IndentationSensitive.getLevel) yield l
        println(runParser[Stream[String, Char], Unit, IndentationSensitive with BracketTracking, Int](ip_, (), "", "", buildIndentSensitiveBracketTracking, 1, 1))
        
        type IndentParser[S <: Stream[_, _], U, A] = ParsecE[S, U, IndentationSensitive, A]
        type S = Stream[String, Char]
        type U = Unit
        val a: IndentParser[S, U, Char]     = char('a') <* skipMany(space)
        val comma: IndentParser[S, U, Char] = char(',') <* skipMany(space)
        val colon: IndentParser[S, U, Char] = char(':') <* skipMany(space)
        
        val ws: IndentParser[S, U, Unit] = skipMany(space <|> newline)
        lazy val line = IndentationSensitive.inLine *> (tryParse(simple) <|> indent)
        lazy val simple = sepEndBy1(a, comma) <* newline
        lazy val suite: IndentParser[S, U, List[Char]] = 
            IndentationSensitive.incLevel *>
            (tryParse(simple <* ws) <|> 
            (IndentationSensitive.block(line, ws) <#> (_.flatten))) <* 
            IndentationSensitive.decLevel
        lazy val indent = ((a <* colon) #> ((xs: List[Char]) => 'a'::xs)) <*>: suite
        
        val prog = many1(line) <#> (_.flatten) <* eof
        
        println(runParser[S, U, IndentationSensitive, List[Char]](prog, (), "", 
"""a
a:
  a, a, a
  a
   a
a
""", IndentationSensitive.build, 1, 1))
        
        lazy val write: String => Parsec[S, U, Unit] = msg => println(msg)
        lazy val w = write("hello world")
        println(runParser[S, U, Unit](w >> w, (), "", "")) 
       
        var j = 0
        start = System.currentTimeMillis()
        val apTest: Parser[String] = pure[S, U, Default, Char => Char => String]((x: Char) => (y: Char) => 
        {
            j += 1
            s"$x$y"
        }) <*> char('a') <*> char('b')
        for (i <- 0 to 10000) runParser[S, U, String](apTest, (), "", "ab")
        println(System.currentTimeMillis() - start)
        println(j)
        
        j = 0
        start = System.currentTimeMillis()
        val apTest_ : Parser[String] = (pure[S, U, Default, Char => Char => String]((x: Char) => (y: Char) => 
        {
            j += 1
            s"$x$y"
        }) <*>: char('a')) <*>: char('b')
        for (i <- 0 to 10000) runParser[S, U, String](apTest_, (), "", "ab")
        println(System.currentTimeMillis() - start)
        println(j)
        
        val xory: Parser[Char] = (for (c <- anyChar; if c == 'x' || c == 'y') yield c) ? "an x or y"
        println(runParser[Stream[String, Char], Unit, Char](xory, (), "", "x"))
        
        val slants: Parser[String] = string("hello world") <\> string("hell")
        println(runParser[Stream[String, Char], Unit, String](slants, (), "", "hell"))
        
        val ops_ : OperatorTable[Stream[String, Char], Unit, BracketTracking, String] = List(Prefixes(List(tok.symbol_("--") #> ((x: String) => s"--($x)"))),
                                                                                             Prefixes(List(tok.symbol_("-") #> ((x: String) => s"-($x)"))))
        lazy val atom_ : ParserB[String] = tok.identifier <|> tok.parens(expr_)
        lazy val expr_ : ParserB[String] = new ExpressionParser[Stream[String, Char], Unit, BracketTracking, String](ops_, atom_).expressionParser
        println(runParser[Stream[String, Char], Unit, BracketTracking, String](expr_, (), "", "---$x", BracketTracking.build, 1, 1))
        
        //val bench: Parser[String] = char('a') <#> ((x: Char) => (y: Char) => x.toString + y.toString) <*> char('b')
        //val bench: Parser[String] = char('a') *> char('b') #> "ab"
        val bench: Parser[List[Char]] = many[S, U, Default, Char](char('a')) <* char('b')
        //val bench2: Parser[Int] = chainr1(char('1').map(_.toInt), char('+') #> ((x: Int) => (y: Int) => x + y))
        start = System.currentTimeMillis()
        //for (i <- 0 to 10000000) runParser[S, U, String](bench, (), "", "ab")
        for (i <- 0 to 10000000) runParser[S, U, List[Char]](bench, (), "", "aaaab")
        //for (i <- 0 to 10000000) runParser[S, U, Int](bench2, (), "", "1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1")
        println(System.currentTimeMillis() - start)
    }
}
