package parsec

import scala.language.existentials
import Combinator.choice
import ParsecE.pure
object Perm 
{
    private[Perm] case class StreamBranch[S <: Stream[_, _], U, A, B](perm: StreamPermParser[S, U, B => A], p: Parsec[S, U, B])
    
    /**The class `StreamPermParser[S, U, A]` represents a permutation parser that, when converted by 
     * its `permute` method, parses streams of `S`, with user state `U` returning an `A` on success.*/
    case class StreamPermParser[S <: Stream[_, _], U, A](x: Option[A], xs: List[StreamBranch[S, U, A, T forSome {type T}]])
    {
        /**The parser `perm.permute` parses a permutation of the parser described by `perm`. An 
         * example parsing an optional string of 'a's, the character 'b' and an optional 'c', is
         * given in the differences section in the wiki documentation for this file.*/
        def permute: Parsec[S, U, A] = 
        {
            val empty: List[Parsec[S, U, A]] = x match
            {
                case Some(x) => List(pure(x))
                case None => List()
            }
            val branch: StreamBranch[S, U, A, T forSome {type T}] => Parsec[S, U, A] = 
            {
                case StreamBranch(perm, p) => 
                    for (x <- p;
                         f <- perm.permute)
                    yield f(x)
            }
            choice(xs.map(branch) ++ empty)
        }
        
        private[Perm] def map[B](f: A => B): StreamPermParser[S, U, B] =
        {
            val mapBranch: StreamBranch[S, U, A, T forSome {type T}] => StreamBranch[S, U, B, T forSome {type T}] =
            {
                case StreamBranch(perm, p) => StreamBranch(perm.map(f compose _), p)
            }
            StreamPermParser(x.map(f), xs.map(mapBranch))
        }
        
        /**The expression `perm <||> p` adds a parser `p` to the permuation parser `perm`. The
         * parser `p` is not allowed to accept empty input: use the optional combinator (`<|?>`) 
         * instead. Returns a new permutation parser that includes `p`.*/
        def <||>[B, C](p: =>Parsec[S, U, B])(implicit g: StreamPermParser[S, U, A] => StreamPermParser[S, U, B => C]): StreamPermParser[S, U, C] =
        {
            val StreamPermParser(_, fs) = g(this)
            val first = StreamBranch(this, p).asInstanceOf[StreamBranch[S, U, C, T forSome {type T}]]
            val insert: StreamBranch[S, U, B => C, T forSome {type T}] => StreamBranch[S, U, C, T forSome {type T}] =
            {
                case StreamBranch(perm_, p_) => StreamBranch(perm_.map(f => ((b: B) => ((t: T forSome {type T}) => f(t)(b)))) <||> p, p_)
            }
            StreamPermParser(None, (first :: fs.map(insert)))
        }
        
        /**The expression `perm <|?> (x, p)` adds parser `p` to the permutation parser `perm`.
         * The parser `p` is optional; if it can not be applied, the default value `x` will be used
         * instead. Returns a new permutation parser that includes the optional parser `p`.*/
        def <|?>[B, C](xp: (B, Parsec[S, U, B]))(implicit g: StreamPermParser[S, U, A] => StreamPermParser[S, U, B => C]): StreamPermParser[S, U, C] =
        {
            val (x, p) = xp
            val StreamPermParser(mf, fs) = g(this)
            val first = StreamBranch(this, p).asInstanceOf[StreamBranch[S, U, C, T forSome {type T}]]
            val insert: StreamBranch[S, U, B => C, T forSome {type T}] => StreamBranch[S, U, C, T forSome {type T}] =
            {
                case StreamBranch(perm_, p_) => StreamBranch(perm_.map(f => ((b: B) => ((t: T forSome {type T}) => f(t)(b)))) <|?> (x, p), p_)
            }
            StreamPermParser(mf.map(_.apply(x)), (first :: fs.map(insert)))
        }
    }
    private[Perm] object StreamPermParser
    {
        def apply[S <: Stream[_, _], U, A, B](f: A => B): StreamPermParser[S, U, A => B] = StreamPermParser(Some(f), List())
    }
    
    implicit class PermLift[S <: Stream[_, _], U, A, B](val f: A => B) extends AnyVal
    {
        /**The expression `f <##> p` creates a fresh permutation parser consisting of parser 
         * `p`. The final result of the permutation parser is the function `f` applied to the 
         * return value of `p`. The parser `p` is not allowed to accept empty input: use the 
         * optional combinator (`<#?>`) instead.
         * 
         * If the function `f` takes more than one parameter, the type variable `B` is instantiated
         * to a function type which combines nicely with the `<||>` combinator. This results in 
         * stylised code where a permutation parser starts with a combining function `f` followed 
         * by the parsers. The function `f` gets its parameters in the order in which the parsers
         * are specified, but the actual input can be in any order.*/
        def <##>(p: =>Parsec[S, U, A]): StreamPermParser[S, U, B] = StreamPermParser(f) <||> p
        
        /**The expression `f <#?> (x, p)` creates a fresh permutation parser consisting of parser
         * `p`. The final result of the permutation parser is the function `f` applied to the return
         * value of `p`, or if it parsed nothing, then the value `x`.*/
        def <#?>(xp: (A, Parsec[S, U, A])): StreamPermParser[S, U, B] = StreamPermParser(f) <|?> xp
    }
    
    def main(args: Array[String]): Unit = 
    {
        import Combinator.many1
        import Char.char
        type S = Stream[String, Char]
        type U = Unit
        val tuple = (a: List[Char]) => (b: Char) => (c: Char) => (a, b, c)
        // This parser parses any permutation of an optional list of as, a b and an optional c
        //val start = System.currentTimeMillis()
        val test = (tuple <#?> (List[Char](), many1(char[String, Unit, Default]('a')))
                          <||> char('b')
                          <|?> ('_', char[String, Unit, Default]('c'))).permute
        //println(System.currentTimeMillis() - start)
        
        println(runParser[S, U, (List[Char], Char, Char)](test, (), "", "bcaaa"))
    }
}