package parsec

import language.existentials

import Combinator.{between, skipMany1, sepBy, sepBy1, option, many1, notFollowedBy, choice}
import Char.{string, satisfy, noneOf, oneOf, digit, hexDigit, octDigit, char, upper}
import Prim.{skipMany, unexpected, many}
import ParsecE.{tryParse, pure, empty, fail, lift2}

final case class LanguageDef[S, U, E](commentStart: String,
                                      commentEnd: String,
                                      commentLine: String,
                                      nestedComments: Boolean,
                                      identStart: ParsecE[Stream[S, Char], U, E, Char],
                                      identLetter: ParsecE[Stream[S, Char], U, E, Char],
                                      opStart: ParsecE[Stream[S, Char], U, E, Char],
                                      opLetter: ParsecE[Stream[S, Char], U, E, Char],
                                      keywords: List[String],
                                      operators: List[String],
                                      caseSensitive: Boolean,
                                      space: ParsecE[Stream[S, Char], U, E, _])
                                        
class TokenParser[S, U, E](languageDef: LanguageDef[S, U, E])
{
    private type SS = Stream[S, Char]
    // Identifiers & Reserved words
    /**This lexeme parser parses a legal identifier. Returns the identifier string. This parser will
     * fail on identifiers that are reserved words (i.e. keywords). Legal identifier characters and
     * keywords are defined in the `LanguageDef` provided to the token parser. An identifier is treated
     * as a single token using `tryParse`.*/
    final def identifier =
        //lexeme(tryParse(ident >>= (name => if (isReservedName(name)) unexpected("keyword " + name) else pure(name))))
        lexeme(tryParse(ident >?> (!isReservedName(_), "keyword " + _)))
    
    /**The lexeme parser `keyword(name)` parses the symbol `name`, but it also checks that the `name`
     * is not a prefix of a valid identifier. A `keyword` is treated as a single token using `tryParse`.*/
    final def keyword(name: String) = lexeme(tryParse(caseString(name) >> notFollowedBy(languageDef.identLetter) ? ("end of " + name)))
    
    final private def caseString(name: String): ParsecE[SS, U, E, String] =
    {
        @inline
        def caseChar(c: Char): ParsecE[SS, U, E, Char] =
            if (c.isLetter) char(c.toLower) <|> char(c.toUpper)
            else char(c)
        def walk(cs: List[Char]): ParsecE[SS, U, E, Unit] = cs match
        {
            case List() => ()
            case c::cs => (caseChar(c) ? name) >> walk(cs)
        }
        if (languageDef.caseSensitive) string(name)
        else walk(name.toList) #> name
    }
    final private val ident = lift2((c: Char) => (cs: List[Char]) => (c::cs).mkString, languageDef.identStart, many(languageDef.identLetter)) ? "identifier"
    final private def isReservedName(name: String): Boolean =
    {
        isReserved(theReservedNames, if (languageDef.caseSensitive) name else name.toLowerCase)
    }
    final private def isReserved(names: List[String], name: String): Boolean =
    {
        def scan(rs: List[String]): Boolean = rs match
        {
            case List() => false
            case r::rs => 
                val cmp = r.compare(name) 
                if (cmp < 0) scan(rs)
                else cmp == 0
        }
        scan(names)
    }
    final private val theReservedNames = 
    {
        if (languageDef.caseSensitive) languageDef.keywords.sortWith((s1, s2) => (s1 compare s2) < 0) 
        else languageDef.keywords.map(_.toLowerCase).sortWith((s1, s2) => (s1 compare s2) < 0)
    }
    
    // Operators & Reserved ops
    /**This lexeme parser parses a legal operator. Returns the name of the operator. This parser
     * will fail on any operators that are reserved operators. Legal operator characters and
     * reserved operators are defined in the `LanguageDef` provided to the token parser. A
     * `userOp` is treated as a single token using `tryParse`.*/
    final def userOp =
        //lexeme(tryParse(oper >>= (name => if (isReservedOp(name)) unexpected("reserved operator " + name) else pure(name))))
        lexeme(tryParse(oper >?> (!isReservedOp(_), "reserved operator " + _)))

    /**This lexeme parser parses a reserved operator. Returns the name of the operator. This parser
     * will fail on any operators that are reserved operators. Legal operator characters and
     * reserved operators are defined in the `LanguageDef` provided to the token parser. A
     * `reservedOp` is treated as a single token using `tryParse`.*/
    final def reservedOp = lexeme(reservedOp_)
    
    /**This non-lexeme parser parses a reserved operator. Returns the name of the operator. This parser
     * will fail on any operators that are reserved operators. Legal operator characters and
     * reserved operators are defined in the `LanguageDef` provided to the token parser. A
     * `reservedOp_` is treated as a single token using `tryParse`.*/
    final def reservedOp_ =
        //tryParse(oper >>= (name => if (!isReservedOp(name)) unexpected("non-reserved operator " + name) else pure(name)))
        tryParse(oper >?> (isReservedOp(_), "non-reserved operator " + _))

    /**The lexeme parser `operator(name)` parses the symbol `name`, but also checks that the `name`
     * is not the prefix of a valid operator. An `operator` is treated as a single token using 
     * `tryParse`.*/
    final def operator(name: String) = lexeme(operator_(name))
    
    /**The lexeme parser `operator_(name)` parses the symbol `name`, but also checks that the `name`
     * is not the prefix of a valid operator. An `operator` is treated as a single token using 
     * `tryParse`.*/
    final def operator_(name: String) = tryParse(string(name) >> notFollowedBy(languageDef.opLetter) ? ("end of " + name))
    
    final private val oper = lift2((c: Char) => (cs: List[Char]) => (c::cs).mkString, languageDef.opStart, many(languageDef.opLetter)) ? "operator"
    final private def isReservedOp(op: String): Boolean = isReserved(languageDef.operators.sortWith((s1, s2) => (s1 compare s2) < 0), op)
    
    // Chars & Strings
    /**This lexeme parser parses a single literal character. Returns the literal character value.
     * This parser deals correctly with escape sequences. The literal character is parsed according
     * to the grammar rules defined in the Haskell report (which matches most programming languages
     * quite closely).*/
    final def charLiteral = lexeme(between(char('\''), char('\'') ? "end of character", characterChar)) ? "character"
    
    /**This lexeme parser parses a literal string. Returns the literal string value. This parser
     * deals correctly with escape sequences and gaps. The literal string is parsed according to
     * the grammar rules defined in the Haskell report (which matches most programming languages
     * quite closely).*/
    final def stringLiteral = lexeme(stringLiteral_)
    
    /**This non-lexeme parser parses a literal string. Returns the literal string value. This parser
     * deals correctly with escape sequences and gaps. The literal string is parsed according to
     * the grammar rules defined in the Haskell report (which matches most programming languages
     * quite closely).*/
    final def stringLiteral_ = 
    {
        between(char[S, U, E]('"') ? "string", char[S, U, E]('"') ? "end of string", many(stringChar)) <#> (_.flatten.mkString)
    }
    
    /**This non-lexeme parser parses a literal string. Returns the literal string value. This parser
     * deals correctly with escape sequences and gaps. The literal string is parsed according to
     * the grammar rules defined in the Haskell report (which matches most programming languages
     * quite closely).*/
    final def rawStringLiteral_ =
    {
        between(char[S, U, E]('"') ? "string", char[S, U, E]('"') ? "end of string", many(stringLetter_)) <#> (_.mkString)
    }
    
    @inline
    final private val decimal_ = number(10, digit)
    final private val charControl = char('^') >> (for (code <- upper[S, U, E]) yield (code - 'A' + 1).toChar)
    final private val charNum = 
    { 
        (decimal_ <|> (char('o') >> number(8, octDigit)) <|> (char('x') >> number(16, hexDigit))) <#> (_.toChar) >?> ((c: Char) => c.toInt <= 0x10FFFF, (_: Char) => "invalid escape sequence")/* >>= (code =>
        {
            if (code > 0x10FFFF) fail("invalid escape sequence")
            else pure(code.toChar)
        })*/
    }
    
    @inline
    final private val ascii2codes = List("BS", "HT", "LF", "VT", "FF", "CR", "SO", "SI", "EM",
                                         "FS", "GS", "RS", "US", "SP")
    @inline
    final private val ascii3codes = List("NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "BEL",
                                         "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB",
                                         "CAN", "SUB", "ESC", "DEL")
    @inline
    final private val ascii2 = List('\u0008', '\u0009', '\n', '\u000b', '\u000c', '\u000d', '\u000e', '\u000f',
                                    '\u0019', '\u001c', '\u001d', '\u001e', '\u001f', '\u0020')
    @inline
    final private val ascii3 = List('\u0000', '\u0001', '\u0002', '\u0003', '\u0004', '\u0005', '\u0006',
                                    '\u0007', '\u0010', '\u0011', '\u0012', '\u0013', '\u0014', '\u0015',
                                    '\u0016', '\u0017', '\u0018', '\u001a', '\u001b', '\u001f')
    @inline
    final private val escMap = ("abfnrtv\\\"\'".toList) zip ("\u0007\b\u000c\n\r\t\u000b\\\"\'".toList)
    @inline
    final private val asciiMap = (ascii3codes ++ ascii2codes) zip (ascii3 ++ ascii2)
    final private val charEsc = choice(escMap.map{case (c, code) => (char[S, U, E](c) #> code)})
    final private val charAscii = choice(asciiMap.map{case (asc, code) => /*tryParse(*/string[S, U, E](asc) #> code/*)*/})
    final private val escapeCode = (charEsc <|> charNum <|> charAscii <\> charControl) ? "escape code"
    final private val charEscape = char('\\') >> escapeCode
    final private val charLetter = satisfy[S, U, E](c => (c != '\'') && (c != '\\') && (c > '\u0016'))
    final private val characterChar = (charLetter <|> charEscape) ? "literal character" 
    
    final private val escapeEmpty = char[S, U, E]('&')
    final private val escapeGap = many1(languageDef.space) >> char('\\') ? "end of string gap"
    final private val stringLetter = satisfy[S, U, E](c => (c != '"') && (c != '\\') && (c > '\u0016'))
    final private val stringLetter_ = satisfy[S, U, E](c => (c != '"') && (c > '\u0016'))
    final private val stringEscape: ParsecE[SS, U, E, Option[Char]] =
    {
        char('\\') >> 
        ((escapeGap #> None) <|> 
         (escapeEmpty #> None) <|> 
         (escapeCode <#> (Some(_))))
    }
    final private val stringChar: ParsecE[SS, U, E, Option[Char]] = 
    {
        ((stringLetter <#> (Option(_))) <|> stringEscape) ? "string character"
    }
    
    // Numbers
    /**This lexeme parser parses a natural number (a positive whole number). Returns the value of
     * the number. The number can specified in `decimal`, `hexadecimal` or `octal`. The number is
     * parsed according to the grammar rules in the Haskell report.*/
    final def natural = lexeme(nat) ? "natural"
    
    /**This lexeme parser parses an integer (a whole number). This parser is like `natural` except
     * that it can be prefixed with a sign (i.e '-' or '+'). Returns the value of the number. The
     * number can be specified in `decimal`, `hexadecimal` or `octal`. The number is parsed
     * according to the grammar rules in the haskell report.*/
    final def integer = lexeme(int) ? "integer"
    
    /**This lexeme parser parses a floating point value. Returns the value of the number. The number
     * is parsed according to the grammar rules defined in the Haskell report.*/
    final def float = lexeme(floating) ? "float"
    
    /**This lexeme parser parses either `natural` or `float`. Returns the value of the number. This
     * parser deals with any overlap in the grammar rules for naturals and floats. The number is 
     * parsed according to the grammar rules defined in the Haskell report.*/
    final def naturalOrFloat = lexeme(natFloat) ? "number"
    
    @inline
    final private val hexadecimal_ = oneOf(List('x', 'X')) >> number(16, hexDigit)
    @inline
    final private val octal_ = oneOf(List('o', 'O')) >> number(8, octDigit)

    //TODO Consider inlining to eliminate >>=?
    // Floats
    final private val fraction = (char('.') >> (for (digits <- many1(digit[S, U, E]) ? "fraction") yield "."+digits.mkString)) ? "fraction"
    final private val exponent_ = (oneOf(List('e', 'E')) >> 
                         (for (sign_ <- (oneOf(List('+', '-')) <#> (_.toString)) </> "";
                               e <- decimal_ ? "exponent")
                          yield 'e' + sign_ + e)) ? "exponent"
    final private val fractExponent: Int => ParsecE[SS, U, E, Double] = n =>
    {
            def readDouble(s: String): ParsecE[SS, U, E, Double] = try pure(s.toDouble) catch { case _ : NumberFormatException => empty }
            (fraction >>= (fract => option("", exponent_) >>= (expo => readDouble(n + fract + expo)))).
        <|> (exponent_ >>= (expo => readDouble(n + expo)))
    }
    final private val floating = decimal_ >>= (n => fractExponent(n))
    final private val decimalFloat: ParsecE[SS, U, E, Either[Int, Double]] = decimal_ >>= (n => option(Left(n), fractFloat(n)))
    final private val fractFloat: Int => ParsecE[SS, U, E, Either[Int, Double]] = n => fractExponent(n) <#> (Right(_))
    final private val zeroNumFloat: ParsecE[SS, U, E, Either[Int, Double]] = 
    {
        ((hexadecimal_ <|> octal_) <#> (Left(_))) <|> decimalFloat <|> fractFloat(0) </> Left(0)
    }
    final private val natFloat = (char('0') >> zeroNumFloat) <|> decimalFloat
    
    // Integers and Naturals
    // Original Parsec defines sign as a lexeme here, this is considered by many as a bug
    @inline
    final private val zeroNumber = (char('0') >> (hexadecimal_ <|> octal_ <|> decimal_ </> 0)) ? ""
    @inline
    final private val nat = zeroNumber <|> decimal_
    @inline
    final private val sign: ParsecE[SS, U, E, Int=>Int] = (char('-') #> ((x: Int) => -x)) <|> (char('+') #> ((x: Int) => x)) </> identity
    @inline
    final private val int = sign <*>: nat
    
    
    /**Parses a positive whole number in the decimal system. Returns the value of the number.*/
    final def decimal = lexeme(decimal_)
    
    /**Parses a positive whole number in the hexadecimal system. The number should be prefixed with 
     * "0x" or "0X". Returns the value of the number.*/
    final def hexadecimal = lexeme(char('0') >> hexadecimal_)
    
    /**Parses a positive whole number in the octal system. The number should be prefixed with "0o"
     * or "0O". Returns the value of the number.*/
    final def octal = lexeme(char('0') >> octal_)
    
    final private def number(base: Int, baseDigit: ParsecE[SS, U, E, Char]): ParsecE[SS, U, E, Int] =
    {
        for (digits <- many1(baseDigit))
        yield digits.foldLeft(0)((x, d) => base*x + d.asDigit)
    }
    
    // White space & symbols
    /**Lexeme parser `symbol(s)` parses `string(s)` and skips trailing white space.*/
    @inline
    final def symbol(name: String) = lexeme[String](string[S, U, E](name))
    
    /**Like `symbol`, but treats it as a single token using `tryParse`*/
    @inline
    final def symbol_(name: String) = tryParse(symbol(name))
    
    /**`lexeme(p)` first applies parser `p` and then the `whiteSpace` parser, returning the value of
     * `p`. Every lexical token (lexeme) is defined using `lexeme`, this way every parse starts at a
     * point without white space. The only point where the `whiteSpace` parser should be called
     * explicitly is the start of the main parser in order to skip any leading white space.*/
    @inline
    final def lexeme[A](p: =>ParsecE[SS, U, E, A]) = p <* whiteSpace
    
    final private lazy val inCommentMulti: ParsecE[SS, U, E, Unit] = 
            string[S, U, E](languageDef.commentEnd).unit().
        <\> (multiLineComment >> inCommentMulti).
        <|> (skipMany1(noneOf[S, U, E](startEnd)) >> inCommentMulti).
        <|> (oneOf(startEnd) >> inCommentMulti) ? "end of comment"
    final private val startEnd: List[Char] = (languageDef.commentEnd + languageDef.commentStart).distinct.toList
    final private lazy val inCommentSingle: ParsecE[SS, U, E, Unit] =
            string[S, U, E](languageDef.commentEnd).unit().
        <\> (skipMany1(noneOf[S, U, E](startEnd)) >> inCommentSingle).
        <|> (oneOf(startEnd) >> inCommentSingle) ? "end of comment"
    final private val inComment = if (languageDef.nestedComments) inCommentMulti else inCommentSingle
    final private val oneLineComment = tryParse(string[S, U, E](languageDef.commentLine)) >> skipMany(satisfy(_!='\n'))
    final private val multiLineComment: ParsecE[SS, U, E, Unit] = tryParse(string[S, U, E](languageDef.commentStart)) >> inComment
    
    /**Parses any white space. White space consists of zero or more occurrences of a `space` (as
     * provided by the `LanguageDef`), a line comment or a block (multi-line) comment. Block
     * comments may be nested. How comments are started and ended is defined in the `LanguageDef`
     * that is provided to the token parser.*/
    final def whiteSpace =
    {
        val noLine = languageDef.commentLine.isEmpty
        val noMulti = languageDef.commentStart.isEmpty
        if (noLine && noMulti) skipMany(space ? "")
        else if (noLine)       skipMany((space <|> multiLineComment) ? "")
        else if (noMulti)      skipMany((space <|> oneLineComment) ? "")
        else                   skipMany((space <|> multiLineComment <|> oneLineComment) ? "")
    }
    
    /**Parses any white space. White space consists of zero or more occurrences of a `space` (as
     * provided by the parameter), a line comment or a block (multi-line) comment. Block
     * comments may be nested. How comments are started and ended is defined in the `LanguageDef`
     * that is provided to the token parser.*/
    final def whiteSpace_(space_ : ParsecE[SS, U, E, _]) = 
    {
        val space = skipMany1(space_)
        val noLine = languageDef.commentLine.isEmpty
        val noMulti = languageDef.commentStart.isEmpty
        if (noLine && noMulti) skipMany(space ? "")
        else if (noLine)       skipMany((space <|> multiLineComment) ? "")
        else if (noMulti)      skipMany((space <|> oneLineComment) ? "")
        else                   skipMany((space <|> multiLineComment <|> oneLineComment) ? "")
    }
    
    /**Parses any comments and skips them, this includes both line comments and block comments.*/
    final def skipComment: ParsecE[SS, U, E, Unit] =
    {
        val noLine = languageDef.commentLine.isEmpty
        val noMulti = languageDef.commentStart.isEmpty
        if (noLine && noMulti) ()
        else if (noLine)       skipMany(multiLineComment ? "")
        else if (noMulti)      skipMany(oneLineComment ? "")
        else                   skipMany(multiLineComment <|> oneLineComment ? "")
    }
    
    @inline
    final private val space = skipMany1(languageDef.space)
    
    // Bracketing
    /**Lexeme parser `parens(p)` parses `p` enclosed in parenthesis, returning the value of `p`.*/
    def parens[A](p: =>ParsecE[SS, U, E, A]) = between(symbol("(") ? "open parenthesis", symbol(")") ? "closing parenthesis" <|> fail("unclosed parentheses"), p)
    
    /**Lexeme parser `braces(p)` parses `p` enclosed in braces ('{', '}'), returning the value of 'p'*/
    def braces[A](p: =>ParsecE[SS, U, E, A]) = between(symbol("{") ? "open brace", symbol("}") ? "matching closing brace" <|> fail("unclosed braces"), p)
    
    /**Lexeme parser `angles(p)` parses `p` enclosed in angle brackets ('<', '>'), returning the
     * value of `p`.*/
    def angles[A](p: =>ParsecE[SS, U, E, A]) = between(symbol("<") ? "open angle bracket", symbol(">") ? "matching closing angle bracket" <|> fail("unclosed angle brackets"), p)
    
    /**Lexeme parser `brackets(p)` parses `p` enclosed in brackets ('[', ']'), returning the value
     * of `p`.*/
    def brackets[A](p: =>ParsecE[SS, U, E, A]) = between(symbol("[") ? "open square bracket", symbol("]") ? "matching closing square bracket" <|> fail("unclosed square brackets"), p)
    
    /**Lexeme parser `semi` parses the character ';' and skips any trailing white space. Returns ";"*/
    @inline
    final def semi = symbol(";") ? "semicolon"
    
    /**Lexeme parser `comma` parses the character ',' and skips any trailing white space. Returns ","*/
    @inline
    final def comma = symbol(",") ? "comma"
    
    /**Lexeme parser `colon` parses the character ':' and skips any trailing white space. Returns ":"*/
    @inline
    final def colon = symbol(":") ? "colon"
    
    /**Lexeme parser `dot` parses the character '.' and skips any trailing white space. Returns "."*/
    @inline
    final def dot = symbol(".") ? "dot"
    
    /**Lexeme parser `semiSep(p)` parses zero or more occurrences of `p` separated by `semi`. Returns
     * a list of values returned by `p`.*/
    @inline
    final def semiSep[A](p: =>ParsecE[SS, U, E, A]) = sepBy(p, semi)
    
    /**Lexeme parser `semiSep1(p)` parses one or more occurrences of `p` separated by `semi`. Returns
     * a list of values returned by `p`.*/
    @inline
    final def semiSep1[A](p: =>ParsecE[SS, U, E, A]) = sepBy1(p, semi)
    
    /**Lexeme parser `commaSep(p)` parses zero or more occurrences of `p` separated by `comma`. 
     * Returns a list of values returned by `p`.*/
    @inline
    final def commaSep[A](p: =>ParsecE[SS, U, E, A]) = sepBy(p, comma)
    
    /**Lexeme parser `commaSep1(p)` parses one or more occurrences of `p` separated by `comma`. 
     * Returns a list of values returned by `p`.*/
    @inline
    final def commaSep1[A](p: =>ParsecE[SS, U, E, A]) = sepBy1(p, comma)
}

trait BracketTracking
{
    var stateLastOpen: Int = 0
}
object BracketTracking
{
    import State._
    import SourcePos.sourceColumn
    class TokenParser[S, U, E](languageDef: LanguageDef[S, U, E with BracketTracking]) extends parsec.TokenParser[S, U, E with BracketTracking](languageDef)
    {
        private type SS = Stream[S, Char]
        /**Lexeme parser `parens(p)` parses `p` enclosed in parenthesis, returning the value of `p`.*/
        override def parens[A](p: =>ParsecE[SS, U, E with BracketTracking, A]) =
        {
            val open = (getPosition >>= (pos => setLastOpen[SS, U, E](sourceColumn(pos)))) >> symbol("(") ? "open parenthesis"
            val close = symbol(")") ? "closing parenthesis"
            val reportUnmatched = fail(getLastOpen[SS, U, E], (col: Int) => s"unclosed parentheses originating at column $col")
            for (openColumn <- getLastOpen[SS, U, E];
                 x <- between(open, close <|> reportUnmatched, p);
                 _ <- setLastOpen(openColumn)) 
            yield x
        }
        
        /**Lexeme parser `braces(p)` parses `p` enclosed in braces ('{', '}'), returning the value of 'p'*/
        override def braces[A](p: =>ParsecE[SS, U, E with BracketTracking, A]) =
        {
            val open = (getPosition >>= (pos => setLastOpen[SS, U, E](sourceColumn(pos)))) >> symbol("(") ? "open parenthesis"
            val close = symbol(")") ? "closing parenthesis"
            val reportUnmatched = fail(getLastOpen[SS, U, E], (col: Int) => s"unclosed parentheses originating at column $col")
            for (openColumn <- getLastOpen[SS, U, E];
                 x <- between(open, close <|> reportUnmatched, p);
                 _ <- setLastOpen(openColumn)) 
            yield x
        }
        
        /**Lexeme parser `angles(p)` parses `p` enclosed in angle brackets ('<', '>'), returning the
         * value of `p`.*/
        override def angles[A](p: =>ParsecE[SS, U, E with BracketTracking, A]) =
        {
            val open = (getPosition >>= (pos => setLastOpen[SS, U, E](sourceColumn(pos)))) >> symbol("(") ? "open parenthesis"
            val close = symbol(")") ? "closing parenthesis"
            val reportUnmatched = fail(getLastOpen[SS, U, E], (col: Int) => s"unclosed parentheses originating at column $col")
            for (openColumn <- getLastOpen[SS, U, E];
                 x <- between(open, close <|> reportUnmatched, p);
                 _ <- setLastOpen(openColumn)) 
            yield x
        }
        
        /**Lexeme parser `brackets(p)` parses `p` enclosed in brackets ('[', ']'), returning the value
         * of `p`.*/
        override def brackets[A](p: =>ParsecE[SS, U, E with BracketTracking, A]) =
        {
            val open = (getPosition >>= (pos => setLastOpen[SS, U, E](sourceColumn(pos)))) >> symbol("(") ? "open parenthesis"
            val close = symbol(")") ? "closing parenthesis"
            val reportUnmatched = fail(getLastOpen[SS, U, E], (col: Int) => s"unclosed parentheses originating at column $col")
            for (openColumn <- getLastOpen[SS, U, E];
                 x <- between(open, close <|> reportUnmatched, p);
                 _ <- setLastOpen(openColumn)) 
            yield x
        }
    }
    
    final def getLastOpen[S <: Stream[_, _], U, E]: ParsecE[S, U, E with BracketTracking, Int] =
    {
        for (state <- getParserState) yield state.stateLastOpen
    }
    
    final def setLastOpen[S <: Stream[_, _], U, E](col: Int): ParsecE[S, U, E with BracketTracking, Unit] =
    {
        updateParserState[S, U, E with BracketTracking](s => 
        {
            val s_ = s.build(Some(s), s.stateInput, s.statePos, s.stateUser)
            s_.stateLastOpen = col
            s_ 
        })
    }
    
    def build[S <: Stream[_, _], U](s: Option[State[S, U, BracketTracking] with BracketTracking], ts: S, pos: SourcePos, u: U): State[S, U, BracketTracking] with BracketTracking =
    {
        val s_ = new State[S, U, BracketTracking](ts, pos, u, build) with BracketTracking
        s match
        {
            case Some(s) => s_.stateLastOpen = s.stateLastOpen
            case None => s_.stateLastOpen = 0
        }
        s_
    }
}
