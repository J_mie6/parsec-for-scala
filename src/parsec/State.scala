package parsec

import parsec._
import ParseError._
import State.StateBuilder

case class State[S <: Stream[_, _], U, E](stateInput: S, statePos: SourcePos, stateUser: U, build: StateBuilder[S, U, E])
object State
{
    type StateBuilder[S <: Stream[_, _], U, E] = (Option[State[S, U, E] with E], S, SourcePos, U) => State[S, U, E] with E
    final def getPosition[S <: Stream[_, _], U, E]: ParsecE[S, U, E, SourcePos] = for (state <- getParserState[S, U, E]) yield statePos(state)
    final def getInput[S <: Stream[_, _], U, E]: ParsecE[S, U, E, S] = for (state <- getParserState[S, U, E]) yield stateInput(state)
    final def setPosition[S <: Stream[_, _], U, E](pos: SourcePos): ParsecE[S, U, E, Unit] = updateParserState[S, U, E](s => s.build(Some(s), s.stateInput, pos, s.stateUser))
    final def setInput[S <: Stream[_, _], U, E](input: S): ParsecE[S, U, E, Unit] = updateParserState[S, U, E](s => s.build(Some(s), input, s.statePos, s.stateUser))
    
    final def getParserState[S <: Stream[_, _], U, E]: ParsecE[S, U, E, State[S, U, E] with E] = updateParserState[S, U, E](identity)
    final def setParserState[S <: Stream[_, _], U, E](st: State[S, U, E] with E): ParsecE[S, U, E, State[S, U, E] with E] = updateParserState[S, U, E](_=>st)
    final def updateParserState[S <: Stream[_, _], U, E](f: State[S, U, E] with E => State[S, U, E] with E) = 
    {
        new ParsecE[S, U, E, State[S, U, E] with E](s => _ => _ => eok => _ =>
        {
            val s_ = f(s)
            Thunk(() => eok(s_)(s_)(newErrorUnknown(s_.statePos)))
        })
    }
    
    final def getState[S <: Stream[_, _], U, E] = getParserState[S, U, E] <#> stateUser
    final def putState[S <: Stream[_, _], U, E](u: U) =
    {
        new ParsecE[S, U, E, Unit](s => _ => _ => eok => _ =>
        {
            val s_ = s.build(Some(s), s.stateInput, s.statePos, u)
            Thunk(() => eok(())(s_)(newErrorUnknown(s_.statePos)))
        })
    }
    final def modifyState[S <: Stream[_, _], U, E](f: U => U) =
    {
        new ParsecE[S, U, E, Unit](s => _ => _ => eok => _ =>
        {
            val s_ = s.build(Some(s), s.stateInput, s.statePos, f(s.stateUser))
            Thunk(() => eok(())(s_)(newErrorUnknown(s_.statePos)))
        })
    }
    
    final def stateInput[S <: Stream[_, _], U](s: State[S, U, _]): S = s.stateInput
    final def statePos[S <: Stream[_, _], U](s: State[S, U, _]): SourcePos = s.statePos
    final def stateUser[S <: Stream[_, _], U](s: State[S, U, _]): U = s.stateUser
}

// This is a special mixin for State, which does absolutely nothing
trait Default
object Default
{
    def build[S <: Stream[_, _], U](s: Option[State[S, U, Default] with Default], ts: S, pos: SourcePos, u: U): State[S, U, Default] with Default =
    {
        new State[S, U, Default](ts, pos, u, build) with Default
    }
}