import scala.annotation.tailrec
import scala.language.implicitConversions
import parsec._
import ParsecE._
import ParseError._
import State._

// We are going to make the assumption that Parsec is not a transformer, since we have access to side-effects ourselves in our functions
// Any state can be stored in the U parameter
package object parsec
{
    type Parsec[S <: Stream[_, _], U, +A] = ParsecE[S, U, Default, A]
    type Parser[+A] = Parsec[Stream[String, Char], Unit, A]
    type Result[S <: Stream[_, _], U, E] = Bounce[Consumed[Reply[S, U, E, _]]]
    
    final def runParsecE[S <: Stream[_, _], U, E, A](p: ParsecE[S, U, E, A], s: State[S, U, E] with E): Consumed[Reply[S, U, E, A]] =
    {
        val cok: A => State[S, U, E] => ParseError => Result[S, U, E] = a => s => err => Chunk(ConsumedA(Ok(a, s, err)))
        val cerr: ParseError => Result[S, U, E] = err => Chunk(ConsumedA(Error(err)))
        val eok: A => State[S, U, E] => ParseError => Result[S, U, E] = a => s => err => Chunk(Empty(Ok(a, s, err)))
        val eerr: ParseError => Result[S, U, E] = err => Chunk(Empty(Error(err)))
        return p.unParser(s)(cok)(cerr)(eok)(eerr).run.asInstanceOf[Consumed[Reply[S, U, E, A]]]
    }
    
    final def runParser[S <: Stream[_, _], U, A](p: ParsecE[S, U, Default, A], u: U, name: String, s: S, line: Int = 1, column: Int = 1): Either[ParseError, A] = runParser[S, U, Default, A](p, u, name, s, Default.build, line, column)
    final def runParser[S <: Stream[_, _], U, E, A](p: ParsecE[S, U, E, A], u: U, name: String, s: S, stateBuilder: StateBuilder[S, U, E], line: Int, column: Int): Either[ParseError, A] =
    {
        try
        {
            val res = runParsecE(p, stateBuilder(None, s, SourcePos(name, line, column), u)) match
            {
                case ConsumedA(r) => r
                case Empty(r) => r 
            }
            res match
            {
                case Ok(x, _, _) => Right(x)
                case Error(err) => Left(err)
            }
        }
        catch
        {
            case e: StackOverflowError => { System.err.println(s"""Exceeded maximum stack depth whilst attempting to parse; 
                                                                  |\t$s
                                                                  |Try increasing the stack depth for the JVM on the command line with "-Xss0000k" (KB) or "-Xss00m" (MB)
                                                                  |replacing 0000 with the desired size in KB (recommended 16MB, but if that isn't enough feel free to increase it!)""".stripMargin);
                                            return Left(unknownError(stateBuilder(None, s, SourcePos(name, line, column), u)))}
        }
    }
    
    sealed trait Bounce[A]
    {
        @tailrec
        final def run: A = this match
        {
            case Thunk(f) => f().run
            case Chunk(x) => x
        }
    }
    case class Thunk[A](f: () => Bounce[A]) extends Bounce[A]
    case class Chunk[A](result: A) extends Bounce[A]
    
    protected sealed trait Consumed[A]
    protected case class ConsumedA[A](a: A) extends Consumed[A]
    protected case class Empty[A](a: A) extends Consumed[A]
    protected sealed trait Reply[S <: Stream[_, _], U, E, A]
    protected case class Ok[S <: Stream[_, _], U, E, A](a: A, s: State[S, U, E], err: ParseError) extends Reply[S, U, E, A]
    protected case class Error[S <: Stream[_, _], U, E, A](err: ParseError) extends Reply[S, U, E, A]
    
    sealed trait Stream[S, X] extends Any
    {
        def uncons(): Option[(X, Stream[S, X])]
    }
    
    implicit class StringStream(val s: String) extends AnyVal with Stream[String, Char]
    {
        def uncons(): Option[(Char, Stream[String, Char])] = 
        {
            if (s.isEmpty()) None
            else Some(s.head, s.tail)
        }
        override def toString = s
    }
    
    implicit class ListStream[X](val xs: List[X]) extends AnyVal with Stream[List[X], X]
    {
        def uncons(): Option[(X, Stream[List[X], X])] = xs match
        {
            case List() => None
            case x::xs => Some(x, xs)
        }
        override def toString = xs.toString
    }
    
    implicit class SeqStream[X](val xs: Seq[X]) extends AnyVal with Stream[Seq[X], X]
    {
        def uncons(): Option[(X, Stream[Seq[X], X])] = xs match
        {
            case Seq() => None
            case x::xs => Some(x, xs)
        }
        override def toString = xs.toString
    }
    
    implicit class StreamStream[X](val xs: scala.Stream[X]) extends AnyVal with Stream[scala.Stream[X], X]
    {
        def uncons(): Option[(X, Stream[scala.Stream[X], X])] = xs match
        {
            case scala.Stream() => None
            case x#::xs => Some(x, xs)
        }
    }
    
    def buildIndentSensitiveBracketTracking[S <: Stream[_, _], U](
        s: Option[State[S, U, IndentationSensitive with BracketTracking] with IndentationSensitive with BracketTracking], 
        ts: S, 
        pos: SourcePos, 
        u: U): State[S, U, IndentationSensitive with BracketTracking] with IndentationSensitive with BracketTracking = 
    {
        val s_ = new State[S, U, IndentationSensitive with BracketTracking](ts, pos, u, buildIndentSensitiveBracketTracking) with IndentationSensitive with BracketTracking
        s match
        {
            case Some(s) => 
                s_.stateLevel = s.stateLevel
                s_.stateRequiredIndent = s.stateRequiredIndent
                s_.levelCache = s.levelCache
                s_.stateLastOpen = s.stateLastOpen
            case None => 
                s_.stateLevel = 0
                s_.stateRequiredIndent = 1
                s_.levelCache = List((0, 1))
                s_.stateLastOpen = 0
        }
        s_
    }
    
    implicit def asParser[S <: Stream[_, _], U, E](u: =>Unit): ParsecE[S, U, E, Unit] = pure[S, U, E, Unit](u)
    implicit def asUnitParser[S <: Stream[_, _], U, E, A](p: ParsecE[S, U, E, A]): ParsecE[S, U, E, Unit] = p.unit()

    def flip[A, B, C](f: A => B => C)(x: B)(y: A) = f(y)(x)
}